#include "view.h"

#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/qi_parse_attr.hpp>
#include <boost/spirit/include/qi_parse_auto.hpp>

#include "html_parser/ast_parser.h"

std::string http_server::view::parse_markup(const std::string& input, const std::string& name)
{
	std::string::const_iterator
		begin = input.begin(), end = input.end();

	ast_node ast;
	std::string outp = "";
	bool r = boost::spirit::qi::phrase_parse(begin, end, p, qi::space, ast);

	if (r && begin == end)
	{
		std::cout << std::string(80, '-') << std::endl; 
		std::cout << "Parsing " << name << " succeeded." << std::endl;
		std::cout << std::string(80, '-') << std::endl;

		ast_parser prn(ast, _model);
		outp =  prn.oss.str();
		//std::cout << outp;

		std::cout << std::string(80, '-') << std::endl;
	}
	else
	{
		std::cout << std::string(80, '-') << std::endl;
		std::cout << "Parsing " << name << " failed, stopped at" << std::endl;
		std::cout << std::string(80, '-') << std::endl;

		ast_parser prn(ast, _model);
		outp = prn.oss.str();
		//std::cout << prn.oss.str();

		std::cout << std::string(80, '-') << std::endl;
		std::cout << "Remaining input" << std::endl;

		std::cout << std::string(begin, end) << std::endl;

		std::cout << std::string(80, '-') << std::endl;
		std::cout << "!!! " << name << " parsing FAILED!" << std::endl;
	}

	return outp;
}