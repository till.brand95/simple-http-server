#ifndef __HTTP_USER_H__
#define __HTTP_USER_H__

#include <string>
#include <functional>

#include "model.h"

namespace http_server
{
	class User : public model
	{
	protected:
		std::string _username;
		std::string _password;
		public:
			User(std::string username, std::string password) : model(), _username(username), _password(password) 
			{
				addAttribute("username", &_username);
			}
			virtual ~User(){}
			
			
			void setUsername(std::string username)
			{
				_username = username;
			}
			
			std::string getUserame()
			{
				return _username;
			}
			
			void setPassword(std::string passwd)
			{
				_password = passwd;
			}
			
			std::string getPassword()
			{
				return _password;
			}
			
			
			
	};
}


#endif