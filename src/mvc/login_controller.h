#ifndef __HTTP_LOGIN_CONTROLLER_H__
#define __HTTP_LOGIN_CONTROLLER_H__

#include <string>
#include <functional>
#include <memory>

#include "controller.h"
#include "user.h"
#include "user_repository.h"

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <iostream>

namespace http_server
{ 
	class login_controller : public controller
	{
		std::map<std::string, std::shared_ptr<User>> logged_in_users;
	protected:
		std::string _loginpage;
		std::string _view;
		std::string _to;
		
		bool is_in_use = false;

		

	public:
		login_controller() : controller()
		{
		}
		virtual ~login_controller(){}

		void setLoginPage(std::string page, std::string view, std::string to)
		{
			_loginpage = page;
			_view = view;
			_to = to;
			is_in_use = true;
			addCallback({ _loginpage }, [&](model* m)->std::string {
				return _view;
				}, HTTP_GET);

			addCallback({ _loginpage }, [&](model* m)->std::string {
				std::string username = m->getAttribute("username")->to_string();
				std::string page = "";
				if (login(username, m->getAttribute("password")->to_string()))
				{
					m->addAttribute("sessionId", getTokenByUsername(username));
					page = _to;
				}
				else
				{
					page = _loginpage;
				}
				m->removeAttribute("username");
				m->removeAttribute("password");
				return "redirect:" + page;
				}, HTTP_POST);
		}

	private:
		std::string getTokenByUsername(std::string username)
		{
			for (auto& user : logged_in_users)
			{
				if (user.second->getUserame() == username)
					return user.first;
			}
			return "";
		}
		bool login(std::string username, std::string password)
		{
			for (auto& user : *user_repository::getInstance())
				if (user->getUserame() == username)
				{
					if (user->getPassword() == password)
					{
						std::stringstream ss;
						auto uuid = boost::uuids::random_generator()();
						ss << uuid;
						std::string token = ss.str();
						int pos = 0;
						while ((pos = token.find('-')) != std::string::npos)
						{
							token.erase(pos, 1);
						}
						logged_in_users.emplace(token,user);
						return true;
					}
				}
			return false;		
		}

		bool logout(std::string token)
		{
			if(isLoggedIn(token))
				logged_in_users.erase(token);
		}
	public:

		bool in_use()
		{
			return is_in_use;
		}

		static std::shared_ptr< login_controller>& getInstance()
		{
			static std::shared_ptr<login_controller> inst;
			if (!inst)
				inst = std::make_shared< login_controller>();
			return inst;
		}

		bool isLoggedIn(std::string token)
		{
			return logged_in_users.count(token) > 0;
		}

		std::shared_ptr<User> getUserByToken(std::string token)
		{
			return isLoggedIn(token) ? logged_in_users.at(token) : 0;
		}

		std::string getLoginpage()
		{
			return _loginpage;
		}
	};
}

#define login_ http_server::login_controller::getInstance()


#endif