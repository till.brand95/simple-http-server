#ifndef __HTTP_REPOSITORY_H__
#define __HTTP_REPOSITORY_H__

#include <string>
#include <functional>
#include <memory>

namespace http_server
{
	template <typename T>
	class repository : std::enable_shared_from_this<repository<T>>
	{
	protected:

		std::vector<std::shared_ptr<T>> repo;

		public:
			repository() {}
			virtual ~repository(){}
			
			
			void add(std::shared_ptr<T> &t)
			{
				repo.push_back(t);
			}
			
			void add(T &t)
			{
				repo.push_back(std::make_shared<T>(t));
			}
			
			void add(T *t)
			{
				repo.push_back(std::make_shared<T>(t));
			}

			void remove(int index)
			{
				if(index < repo.size())
					repo.erase(repo.begin() + index);
			}

			std::shared_ptr<T>& get(int i)
			{
				if (i < repo.size())
					return repo.at(i);
			}
			
			static std::shared_ptr<repository<T>> getInstance()
			{
				static std::shared_ptr<repository<T>> inst;
				if (!inst)
					inst = std::make_shared<repository<T>>();

				return inst;
			}

			auto begin() { return repo.begin(); }
			auto begin() const { return repo.begin(); }
			auto end() { return repo.end(); }
			auto end() const { return repo.end(); }
			
	};
}


#endif