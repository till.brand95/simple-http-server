#ifndef __HTTP_USER_REPOSITORY_H__
#define __HTTP_USER_REPOSITORY_H__

#include <string>
#include <functional>

#include "repository.h"
#include "user.h"

namespace http_server
{
	class user_repository : public repository<User>
	{
	protected:

		

	public:
		user_repository() {}
		virtual ~user_repository(){}

		std::shared_ptr<User>& getUserByName(std ::string username)
		{
			std::shared_ptr<User> usr = 0;
			for (auto& user : repo)
			{
				if (user->getUserame() == username)
				{
					usr = user;
					break;
				}
			}
			return usr;
		}
		
			
	};
}


#endif