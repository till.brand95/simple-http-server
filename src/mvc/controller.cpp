#include "controller.h"

namespace http_server
{
	std::map<std::string, std::shared_ptr<controller::site_callback_holder>> controller::sites = std::map<std::string, std::shared_ptr<controller::site_callback_holder>>();

	void controller::addCallback(std::vector<std::string> sitess, std::function<std::string(model*)> callback, bool auth)
	{
		for (auto& site : sitess)
		{
			std::shared_ptr<site_callback_holder> sc;
			if (sites.count(site) > 0)
			{
				sc = sites.at(site);
			}
			else
				sc = std::make_shared<site_callback_holder>();
			sc->get_callback.callback = callback;
			sc->get_callback.authentificate = auth;
			sc->post_callback.callback = callback;
			sc->post_callback.authentificate = auth;
			if (sites.count(site) == 0)
			{
				sites.emplace(site, sc);
			}
		}
	}
	
	void controller::addCallback(std::vector<std::string> sitess, std::function<std::string(model*)> callback, boost::beast::http::verb _method, bool auth)
	{
		for (auto& site : sitess)
		{
			std::shared_ptr<site_callback_holder> sc;
			if (sites.count(site) > 0)
			{
				sc = sites.at(site);
			}
			else
				sc = std::make_shared<site_callback_holder>();

			if (_method == boost::beast::http::verb::get)
			{
				sc->get_callback.callback = callback;
				sc->get_callback.authentificate = auth;
			}
			if (_method == boost::beast::http::verb::post)
			{
				sc->post_callback.callback = callback;
				sc->post_callback.authentificate = auth;
			}

			if (sites.count(site) == 0)
			{
				sites.emplace(site, sc);
			}
			
		}
	}
}