#ifndef __HTTP_CONTROLLER_H__
#define __HTTP_CONTROLLER_H__

#include <functional>
#include <string>
#include <map>
#include <vector>
#include <memory>
#include <boost/beast/http.hpp>

#include "model.h"

#define HTTP_GET boost::beast::http::verb::get
#define HTTP_POST boost::beast::http::verb::post

namespace http_server
{
	class controller
	{
		public:
			struct site_callback
			{
				std::function<std::string(model*)> callback;
				bool authentificate;

				std::string operator()(model*m)
				{
					return callback(m);
				}
			};

			struct site_callback_holder
			{
				site_callback get_callback;
				site_callback post_callback;
			};


			static std::map<std::string, std::shared_ptr<site_callback_holder>> sites;


			static site_callback getCallback(std::string site, boost::beast::http::verb _method = HTTP_GET)
			{
				if (sites.count(site) == 0)
					return site_callback{};
				if(_method == HTTP_GET)
					return sites.at(site)->get_callback;
				else if (_method == HTTP_POST)
					return sites.at(site)->post_callback;
				return site_callback{};
			}

			
			
		public:
			controller(){}
			virtual ~controller(){}

			void addCallback(std::vector<std::string> sitess, std::function<std::string(model*)> callback, bool auth = false);

			void addCallback(std::vector<std::string> sitess, std::function<std::string(model*)> callback, boost::beast::http::verb _method, bool auth = false);
			
	};
}


#endif