#ifndef __CALLABLE_FUNCTION_H__
#define __CALLABLE_FUNCTION_H__

#include <functional>
#include <memory>
#include <vector>


#include "argument_setter.h"
#include "element_type.h"
#include "abstract_models.h"


namespace http_server 
{

	class abstract_element;

	template<typename Test, template<typename...> class Ref>
	struct is_specialization : std::false_type {};

	template<template<typename...> class Ref, typename... Args>
	struct is_specialization<Ref<Args...>, Ref> : std::true_type {};

	template<typename V>
	struct vector_info
	{
		typedef V value_type;
		std::vector<V> dat;
	};

	class abstract_argument_setter;

	class abstract_function
	{
	public:
	abstract_function(){}
	virtual ~abstract_function(){}
		virtual std::shared_ptr<abstract_element> call() = 0;
		virtual void setArgument(size_t index, int& i) = 0;
		virtual void setArgument(size_t index, double& i) = 0;
		virtual void setArgument(size_t index, std::string& i) = 0;
		virtual void setArgument(size_t index, abstract_element_type* i) = 0;
		virtual void setArgument(size_t index, std::shared_ptr< abstract_element_type > i) = 0;
		virtual void setArgument(size_t index, model_element* i) = 0;
		virtual void setArgument(size_t index, std::shared_ptr< model_element > i) = 0;
		virtual void setArgument(size_t index, unsigned int& i) = 0;
		virtual void setArgument(size_t index, long& i) = 0;
		virtual void setArgument(size_t index, unsigned long& i) = 0;
		virtual void setArgument(size_t index, long long& i) = 0;
		virtual void setArgument(size_t index, unsigned long long& i) = 0;
	};

	template<typename ...Arguments>
	class abstract_callable_function : public abstract_function
	{
	protected:
		std::tuple<Arguments...> args;
		argument_setter<sizeof...(Arguments)> arg_setter;
	public:
		abstract_callable_function() : abstract_function()
		{
		}

		virtual ~abstract_callable_function()
		{

		}

		template<typename T>
		void setArgument(size_t index, T& val)
		{
			arg_setter.setValue(index, args, val);
		}

		void setArgument(size_t index, unsigned int& i) {
			setArgument<unsigned int>(index, i);
		}

		void setArgument(size_t index, long& i) {
			setArgument<long>(index, i);
		}
		void setArgument(size_t index, unsigned long& i) {
			setArgument<unsigned long>(index, i);
		}

		void setArgument(size_t index, long long& i) {
			setArgument<long long>(index, i);
		}
		void setArgument(size_t index, unsigned long long& i) {
			setArgument<unsigned long long>(index, i);
		}

		void setArgument(size_t index, int& i) {
			setArgument<int>(index, i);
		}
		void setArgument(size_t index, double& i) {
			setArgument<double>(index, i);
		}
		void setArgument(size_t index, std::string& i) {
			setArgument<std::string>(index, i);
		}

		void setArgument(size_t index, abstract_element_type* i)
		{
			arg_setter.setValue(index, args, i->getValue());
		}

		void setArgument(size_t index, std::shared_ptr < abstract_element_type> i)
		{
			arg_setter.setValue(index, args, i->getValue());
		}

		void setArgument(size_t index, std::shared_ptr<model_element> i)
		{
			setArgument< std::shared_ptr<model_element> >(index, i);
		}

		void setArgument(size_t index, model_element* i)
		{
			setArgument< model_element* >(index, i);
		}
	};

	template<typename Ret, typename ... Arguments>
	class callable_function : public abstract_callable_function<Arguments...>
	{
	protected:
		std::function<Ret(Arguments...)> func;
	public:
		callable_function(std::function<Ret(Arguments...)> _func) : abstract_callable_function<Arguments...>(), func(_func) {}
		~callable_function() { }

		template<typename ... Args>
		Ret apply_from_tuple(std::tuple<Args...>& t)
		{
			std::size_t constexpr tSize = std::tuple_size<typename std::remove_reference<decltype(t)>::type>::value;
			return apply_tuple_impl(std::forward<decltype(t)>(t), std::make_index_sequence<tSize>());
		}

		template<typename R, size_t ...S >
		Ret apply_tuple_impl(R& t, std::index_sequence<S...>)
		{

			return func(std::get<S>(std::forward<decltype(t)>(t))...);
		}

		std::shared_ptr<abstract_element> call_r()
		{

				if (!abstract_callable_function<Arguments...>::arg_setter.isSet())
					return 0;
				Ret val = apply_from_tuple(abstract_callable_function<Arguments...>::args);
				abstract_callable_function<Arguments...>::arg_setter.reset();
				auto ret = std::make_shared< element_type<Ret>>(&val, true);
				return ret;
			
		}

		std::shared_ptr<abstract_element> call()
		{
			return call_r();
		}
	};

	template<class T, typename ... Arguments>
	class callable_function<std::vector<T>, Arguments...>  : public abstract_callable_function<Arguments...>
	{
	protected:
		typedef std::vector<T> Ret;
		std::function<Ret(Arguments...)> func;
	public:
		callable_function(std::function<Ret(Arguments...)> _func) : abstract_callable_function<Arguments...>(), func(_func) {}
		~callable_function() { }

		template<typename ... Args>
		Ret apply_from_tuple(std::tuple<Args...>& t)
		{
			std::size_t constexpr tSize = std::tuple_size<typename std::remove_reference<decltype(t)>::type>::value;
			return apply_tuple_impl(std::forward<decltype(t)>(t), std::make_index_sequence<tSize>());
		}

		template<typename R, size_t ...S >
		Ret apply_tuple_impl(R& t, std::index_sequence<S...>)
		{

			return func(std::get<S>(std::forward<decltype(t)>(t))...);
		}

		std::shared_ptr<abstract_element> call_r()
		{

				if (!abstract_callable_function<Arguments...>::arg_setter.isSet())
					return 0;
				Ret val = apply_from_tuple(abstract_callable_function<Arguments...>::args);
				abstract_callable_function<Arguments...>::arg_setter.reset();
				std::shared_ptr<abstract_element> ret = std::make_shared< element_type_vector<T>>(val);

				return ret;
		}

		std::shared_ptr<abstract_element> call()
		{
			return call_r();
		}
	};

	template<typename ... Arguments>
	class callable_function<void, Arguments...> : public abstract_callable_function<Arguments...>
	{
	protected:
		std::function<void(Arguments...)> func;
	public:
		callable_function(std::function<void(Arguments...)> _func) : abstract_callable_function<Arguments...>(), func(_func) {}
		~callable_function() { }

		template<typename ... Args>
		void apply_from_tuple(std::tuple<Args...>& t)
		{
			std::size_t constexpr tSize = std::tuple_size<typename std::remove_reference<decltype(t)>::type>::value;
			apply_tuple_impl(std::forward<decltype(t)>(t), std::make_index_sequence<tSize>());
		}

		template<typename R, size_t ...S >
		void apply_tuple_impl(R& t, std::index_sequence<S...>)
		{

			func(std::get<S>(std::forward<decltype(t)>(t))...);
		}

		std::shared_ptr<abstract_element> call()
		{

			if (!abstract_callable_function<Arguments...>::arg_setter.isSet())
				return 0;
			apply_from_tuple(abstract_callable_function<Arguments...>::args);
			abstract_callable_function<Arguments...>::arg_setter.reset();

			return 0;
		}
	};

	template<typename ... Arguments>
	class callable_function<model_element*, Arguments...> : public abstract_callable_function<Arguments...>
	{
	protected:
		typedef model_element* Ret;
		std::function<Ret(Arguments...)> func;
	public:
		callable_function(std::function<Ret(Arguments...)> _func) : abstract_callable_function<Arguments...>(), func(_func) {}
		~callable_function() { }

		template<typename ... Args>
		Ret apply_from_tuple(std::tuple<Args...>& t)
		{
			std::size_t constexpr tSize = std::tuple_size<typename std::remove_reference<decltype(t)>::type>::value;
			return apply_tuple_impl(std::forward<decltype(t)>(t), std::make_index_sequence<tSize>());
		}

		template<typename R, size_t ...S >
		Ret apply_tuple_impl(R& t, std::index_sequence<S...>)
		{

			return func(std::get<S>(std::forward<decltype(t)>(t))...);
		}

		std::shared_ptr<abstract_element> call()
		{

			if (!abstract_callable_function<Arguments...>::arg_setter.isSet())
				return 0;
			Ret val = apply_from_tuple(abstract_callable_function<Arguments...>::args);
			abstract_callable_function<Arguments...>::arg_setter.reset();
			std::shared_ptr<abstract_element> ret = 0;

			ret = std::make_shared< model_element>((model_element*)&val);
			return ret;
		}
	};

	

	
}
#endif