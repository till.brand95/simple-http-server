#ifndef __ARGUMENT_SETTER_H__
#define __ARGUMENT_SETTER_H__

#include <tuple>

namespace http_server{



class abstract_argument_setter
	{
	protected:
		bool set = false;
	public:
		abstract_argument_setter(){}
		virtual ~abstract_argument_setter(){}

		virtual bool isSet() {
			if (getPrevious() == nullptr)
			{
				return true;
			}
			else
				return set && getPrevious()->isSet();
		}
		virtual void reset() {
			set = false; if (getPrevious() != nullptr) { getPrevious()->reset(); }
		}

		virtual abstract_argument_setter* getPrevious() = 0;
	};


	template<size_t argsize>
	class argument_setter : public abstract_argument_setter
	{
	public:
		argument_setter(){}

		abstract_argument_setter* getPrevious() { return nullptr; }
		bool isSet() {
			return false;
		}

	};

	template<>
	class argument_setter<0> : public abstract_argument_setter
	{
	public:
		argument_setter() :abstract_argument_setter() {}

		template<typename T, typename ...Args>
		void setValue(size_t idx, std::tuple<Args...>& tuple, T& i)
		{
			set = true;
		}

		template<typename ...Args>
		void setValue(size_t idx, std::tuple<Args...>& tuple, void* i)
		{

		}

		abstract_argument_setter* getPrevious()
		{
			return nullptr;
		}

	};

#define ARG_SETTER(_X_) \
	template<>  \
	class argument_setter<_X_> : public abstract_argument_setter \
	{ \
	public: \
		argument_setter() :abstract_argument_setter(sub) {} \
		argument_setter<_X_-1> sub = argument_setter<_X_-1>(); \
		template<typename T, typename ...Args> \
		void setValue(size_t idx, std::tuple<Args...>& tuple, T& val) \
		{ \
			if (idx == _X_-1) \
			{ \
			    typedef typename std::tuple_element<_X_-1, std::tuple<Args...>>::type curtp; \
				std::get<_X_-1>(tuple) = (curtp&)val; \
				set = true; \
			} \
			else \
				sub.setValue(idx, tuple, val); \
		} \
		template<typename T, typename ...Args> \
		void setValue(size_t idx, std::tuple<Args...>& tuple, T* i) \
		{ \
			if (idx == _X_-1) \
			{ \
			    typedef typename std::tuple_element<_X_-1, std::tuple<Args...>>::type curtp; \
				std::get<_X_-1>(tuple) = (curtp&)i; \
				set = true; \
			} \
			else \
				sub.setValue(idx, tuple, i); \
		} \
		abstract_argument_setter* getPrevious() \
		{ \
			return &sub; \
		} \
		template<typename ...Args> \
		void setValue(size_t idx, std::tuple<Args...>& tuple, void* val) \
		{ \
			if (idx == _X_-1) \
			{ \
			    typedef typename std::tuple_element<_X_-1, std::tuple<Args...>>::type curtp; \
				std::get<_X_-1>(tuple) = *((curtp*&)val); \
				set = true; \
			} \
			else \
				sub.setValue(idx, tuple, val); \
		} \
	};

	ARG_SETTER(1);
	ARG_SETTER(2);
	ARG_SETTER(3);
	ARG_SETTER(4);
	ARG_SETTER(5);
	ARG_SETTER(6);
	ARG_SETTER(7);
	ARG_SETTER(8);
	ARG_SETTER(9);
	}
	#endif