#ifndef __ELEMNT_TYPE_H__
#define __ELEMNT_TYPE_H__

#include <iostream>
#include <string>
#include <map>
#include <typeinfo>
#include <sstream>
#include <type_traits>

#include <boost/assert.hpp>

#include "abstract_models.h"
#include <vector>

namespace http_server 
{
	class abstract_element;
	class abstract_element_type;

	template<typename T, bool is_abstarct_element>
	struct vector_helper
	{

	};
	
	template<class T>
	class element_type : public abstract_none_iterable_element
	{
	protected:
		T* element;
		public:
			element_type(T* ptr, bool owner = false): abstract_none_iterable_element(owner), element(ptr)
			{
				if (_owner)
					element = new T(*ptr);
			}

			element_type(T ptr) : abstract_none_iterable_element(true)
			{
				element = new T(ptr);
			}

			element_type(std::shared_ptr<T> ptr, bool owner = false) : abstract_none_iterable_element(owner), element(ptr.get())
			{
				if (_owner)
					element = new T(*ptr);
			}


			~element_type()
			{
				if (_owner && element)
					delete element;
			}

			

			void setValue(void* val) override
			{
				*element = *(reinterpret_cast<T*>(val));
			}

			void* getValue() override
			{
				return element;
			}

			element_type<T>* get() {
				return this;
			}

			std::string to_string()
			{
				std::stringstream ss;
				ss << *element;
				return ss.str();
			}

			virtual abstract_none_iterable_element* asElement() override {
				return !iterable() ? (abstract_none_iterable_element*)this : nullptr;
			}
			virtual abstract_iterable_element* asList() override {
				return nullptr;
			}
			
	};

	template<typename T>
	class element_type_vector : public abstract_iterable_element
	{
	protected:
		std::vector<T>* items;
	public:
		element_type_vector(std::vector <T>* ptr, bool owner = false) : abstract_iterable_element(owner), items(ptr)
		{
			if (_owner)
				items = new std::vector<T>(*ptr);
		}

		element_type_vector(std::vector <T> ptr) : abstract_iterable_element(true)
		{
			items = new std::vector<T>(ptr);
		}

		~element_type_vector()
		{
			if (_owner && items)
				delete items;
		}

		std::string to_string() override
		{
			return vector_helper<T, std::is_base_of<abstract_element, T>::value>::get_string(items);
		}


		std::shared_ptr<abstract_element> get(size_t pos) override
		{
			return vector_helper<T, std::is_base_of<abstract_element, T>::value>::get_r(pos, items);
		}

		size_t size() override { return items->size(); }

		void setValue(void* val) override
		{
			*items = *(reinterpret_cast<decltype(items)>(val));
		}

		void* getValue() override
		{
			return items;
		}

		virtual abstract_none_iterable_element* asElement() override {
			return nullptr;
		}
		virtual abstract_iterable_element* asList() override {
			return iterable() ? (abstract_iterable_element*)this : nullptr;
		}
	};

	template<typename T>
	class element_type_vector<std::shared_ptr<T>> : public abstract_iterable_element
	{
	protected:
		std::vector<std::shared_ptr<T>>* items;
	public:
		element_type_vector(std::vector <std::shared_ptr<T>>* ptr, bool owner = false) : abstract_iterable_element(owner), items(ptr)
		{
			if (_owner)
				items = new std::vector<std::shared_ptr<T>>(*ptr);
		}

		element_type_vector(std::vector <std::shared_ptr<T>> ptr) : abstract_iterable_element(true)
		{
			items = new std::vector<std::shared_ptr<T>>(ptr);
		}

		~element_type_vector()
		{
			if (_owner && items)
				delete items;
		}

		std::string to_string() override
		{
			return vector_helper< T, std::is_base_of<abstract_element, T>::value>::get_string(items);
		}


		std::shared_ptr<abstract_element> get(size_t pos) override
		{
			return vector_helper<T, std::is_base_of<abstract_element, T>::value>::get_r(pos, items);
		}

		size_t size() override { return items->size(); }

		void setValue(void* val) override
		{
			*items = *(reinterpret_cast<decltype(items)>(val));
		}

		void* getValue() override
		{
			return items;
		}

		virtual abstract_none_iterable_element* asElement() override {
			return nullptr;
		}
		virtual abstract_iterable_element* asList() override {
			return iterable() ? (abstract_iterable_element*)this : nullptr;
		}
	};

	template<typename T>
	struct vector_helper<T, true>
	{
		static std::string get_string(std::vector<T>* items)
		{
			std::stringstream ss;
			ss << "[";
			for (int i = 0; i < items->size(); i++)
				ss << items->at(i).to_string() << ",";
			ss << "]";
			return ss.str();
		}

		static std::shared_ptr<abstract_element> get_r(size_t pos, std::vector<T>* items)
		{
			if (pos < items->size())
			{
				return std::make_shared<T>(new T(items->at(pos)));
			}
			return nullptr;
		}

		static std::string get_string(std::vector<T*>* items)
		{
			std::stringstream ss;
			ss << "[";
			for (int i = 0; i < items->size(); i++)
				ss << items->at(i)->to_string() << ",";
			ss << "]";
			return ss.str();
		}

		static std::shared_ptr<abstract_element> get_r(size_t pos, std::vector<T*>* items)
		{
			if (pos < items->size())
			{
				return std::make_shared<T>(new T(*items->at(pos)));
			}
			return nullptr;
		}

		static std::string get_string(std::vector<std::shared_ptr<T>>* items)
		{
			std::stringstream ss;
			ss << "[";
			for (int i = 0; i < items->size(); i++)
				ss << items->at(i)->to_string() << ",";
			ss << "]";
			return ss.str();
		}

		static std::shared_ptr<abstract_element> get_r(size_t pos, std::vector<std::shared_ptr<T>>* items)
		{
			if (pos < items->size())
			{
				return items->at(pos);
			}
			return nullptr;
		}
	};

	template<typename T>
	struct vector_helper<T, false>
	{
		static std::string get_string(std::vector<T>* items)
		{
			std::stringstream ss;
			ss << "[";
			for (int i = 0; i < items->size(); i++)
				ss << items->at(i) << ",";
			ss << "]";
			return ss.str();
		}

		static std::shared_ptr<abstract_element> get_r(size_t pos, std::vector<T>* items)
		{
			if (pos < items->size())
			{
				return std::make_shared <element_type<T>>(items->at(pos));
			}
			return nullptr;
		}

		static std::string get_string(std::vector<T*>* items)
		{
			std::stringstream ss;
			ss << "[";
			for (int i = 0; i < items->size(); i++)
				ss << *items->at(i) << ",";
			ss << "]";
			return ss.str();
		}

		static std::shared_ptr<abstract_element> get_r(size_t pos, std::vector<T*>* items)
		{
			if (pos < items->size())
			{
				return std::make_shared <element_type<T>>(*items->at(pos));
			}
			return nullptr;
		}

		static std::string get_string(std::vector<std::shared_ptr<T>>* items)
		{
			std::stringstream ss;
			ss << "[";
			for (int i = 0; i < items->size(); i++)
				ss << *items->at(i) << ",";
			ss << "]";
			return ss.str();
		}

		static std::shared_ptr<abstract_element> get_r(size_t pos, std::vector<std::shared_ptr<T>>* items)
		{
			if (pos < items->size())
			{
				return std::make_shared <element_type<T>>(*items->at(pos));
			}
			return nullptr;
		}
	};
}
#endif