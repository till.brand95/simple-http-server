#ifndef __ABSTRACT_MODELS_H__
#define __ABSTRACT_MODELS_H__

#include <string>
#include <memory>

#include "model_implementations.h"

namespace http_server 
{
	class model_element;
	class abstract_element_type;
	class abstract_iterable_element;
	class abstract_none_iterable_element;

	class abstract_element : protected std::enable_shared_from_this< abstract_element>
	{
	public:
		abstract_element() {}
		virtual ~abstract_element() {}
		virtual bool isClass() = 0;

		virtual model_element* toModelElement() = 0;
		virtual abstract_element_type* toElementType() = 0;

		virtual std::string to_string() = 0;
	};

	

	class abstract_element_type: public abstract_element
	{
	protected:
		bool _owner;
	public:
		abstract_element_type(bool owner) : abstract_element(), _owner(owner) {}
		virtual ~abstract_element_type() {}
		virtual void setValue(void* val) = 0;
		virtual void* getValue() = 0;
		
		bool isClass() override { return false; }
		virtual bool iterable() = 0;
		void setOwner(bool owner) { _owner = owner; }
		bool getOwner() { return _owner; }

		virtual abstract_none_iterable_element* asElement() = 0;
		virtual abstract_iterable_element* asList() = 0;

	};

	class abstract_none_iterable_element : public abstract_element_type
	{
	public:
		abstract_none_iterable_element(bool owner) : abstract_element_type(owner)
		{

		}
		~abstract_none_iterable_element() {}
		bool iterable() override { return false; }

		abstract_element_type* toElementType() override {
			if (!isClass()) { return this; }return nullptr;
		}

		model_element* toModelElement() override { return nullptr; }

	};

	class abstract_iterable_element : public abstract_element_type
	{
	public:
		abstract_iterable_element(bool owner) : abstract_element_type(owner)
		{

		}
		~abstract_iterable_element() {}

		bool iterable() override { return true; }

		abstract_element_type* toElementType() override {
			if (!isClass()) { return this; }return nullptr;
		}

		model_element* toModelElement() override { return nullptr; }

		virtual std::shared_ptr<abstract_element> get(size_t pos) = 0;

		virtual size_t size() = 0;

	};
	
}
#endif