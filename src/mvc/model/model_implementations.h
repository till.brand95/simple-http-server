#ifndef __MODEL_IMPL_H__
#define __MODEL_IMPL_H__

#include <iostream>
#include <string>
#include <map>
#include <typeinfo>
#include <sstream>
#include <type_traits>

#include <boost/assert.hpp>

#include "abstract_models.h"
#include "callable_function.h"
#include "element_type.h"
#include <vector>

namespace http_server 
{
	class abstract_element;
	class abstract_element_type;
	class model_element;
	class abstract_function;

	template<typename T, bool is_abstract_element>
	struct attribute_helper
	{

	};

	template<typename T>
	struct vec_attribute_helper
	{
		std::map<std::string, std::shared_ptr<abstract_element>>& subelements;
		vec_attribute_helper(std::map<std::string, std::shared_ptr<abstract_element>>& _subelements) : subelements(_subelements)
		{

		}
		void addAttribute(std::string name, std::vector<T> value)
		{
			if (hasAttribute(name))
				subelements.erase(name);
			subelements.emplace(name, std::make_shared < element_type_vector<T>>(value));
		}

		void addAttribute(std::string name, std::vector<T>* value)
		{
			if (hasAttribute(name))
				subelements.erase(name);
			subelements.emplace(name, std::make_shared < element_type_vector<T>>(value));
		}

		void addAttribute(std::string name, std::vector<std::shared_ptr<T>>* value)
		{
			if (hasAttribute(name))
				subelements.erase(name);
			subelements.emplace(name, std::make_shared < element_type_vector<T>>(value));
		}
		

		bool hasAttribute(std::string name)
		{
			return subelements.count(name) > 0;
		}
	};

	template<typename T>
	struct attribute_helper<T, false>
	{
		std::map<std::string, std::shared_ptr<abstract_element>>& subelements;
		attribute_helper(std::map<std::string, std::shared_ptr<abstract_element>>& _subelements) : subelements(_subelements)
		{

		}

		void addAttribute(std::string name, T* value)
		{
			if (hasAttribute(name))
				subelements.erase(name);
			subelements.emplace(name, std::make_shared < element_type<T>>(value));
		}

		void addAttribute(std::string name, std::shared_ptr<T> value)
		{
			if (hasAttribute(name))
				subelements.erase(name);
			subelements.emplace(name, std::make_shared<element_type<T>>(value));
		}

		void addAttribute(std::string name, T value)
		{
			if (hasAttribute(name))
				subelements.erase(name);
			subelements.emplace(name, std::make_shared<element_type<T>>(value));
		}

		void addAttribute(std::string name)
		{
			if (hasAttribute(name))
				subelements.erase(name);
			subelements.emplace(name, nullptr);
		}

		bool hasAttribute(std::string name)
		{
			return subelements.count(name) > 0;
		}
	};

	template<typename T>
	struct attribute_helper<T, true>
	{
		std::map<std::string, std::shared_ptr<abstract_element>>& subelements;
		attribute_helper(std::map<std::string, std::shared_ptr<abstract_element>>& _subelements) : subelements(_subelements)
		{

		}

		void addAttribute(std::string name, T* value)
		{
			if (hasAttribute(name))
				subelements.erase(name);
			subelements.emplace(name, std::make_shared<T>(value));
		}

		void addAttribute(std::string name, std::shared_ptr<T> value)
		{
			if (hasAttribute(name))
				subelements.erase(name);
			subelements.emplace(name, value);
		}

		bool hasAttribute(std::string name)
		{
			return subelements.count(name) > 0;
		}
	};



	class model_element : public abstract_element
	{
	protected:
		std::map<std::string, std::shared_ptr<abstract_element>> subelements;
		std::map<std::string, std::shared_ptr<abstract_function>> object_functions;
	public:
		model_element() : abstract_element() {}
		virtual ~model_element() 
		{
		
		}

		std::string to_string()
		{
			std::stringstream ss;
			ss << "{";
			for (auto& el : subelements)
			{
				ss << el.first << ":\"" << el.second->to_string() << "\",";
			}
			ss << "}";
			return ss.str();
		}

		template<typename F, typename ... Args>
		void addFunction(std::string name, std::function<F(Args...)> f)
		{
			auto _it = callable_function<F, Args...>(f);
			object_functions.emplace(name, std::make_shared<decltype(_it)>(_it));
		}

		std::shared_ptr<abstract_function> getFunction(std::string name)
		{
			if (object_functions.count(name) > 0)
				return object_functions.at(name);
			return 0;
		}

		template<typename T>
		void setFunctionParameter(std::string name, size_t index, T val)
		{
			if (object_functions.count(name) > 0)
			{
				auto func = object_functions.at(name);
				func->setArgument(index, val);
			}
		}

		template<typename T>
		T CallFunction(std::string name)
		{
			try {
				if (object_functions.count(name) > 0)
				{
					auto func = object_functions.at(name);
					auto ret = func->call();
					if (!ret)
						return T();
					if (!ret->isClass())
						return *reinterpret_cast<T*>(ret->toElementType()->getValue());
					else
						return ret->toModelElement();
					return T();
				}
			}
			catch (std::exception e)
			{
				std::cout << e.what();
			}
		}

		void removeAttribute(std::string name)
		{
			if(hasAttribute(name))
				subelements.erase(name);
		}

		template<typename T>
		void addAttribute(std::string name, T value)
		{
			attribute_helper<T, std::is_base_of<abstract_element, T>::value> ah(subelements);
			ah.addAttribute(name, value);
		}

		template<typename T>
		void addAttribute(std::string name, std::vector<T> value)
		{
			vec_attribute_helper< T> ah(subelements);
			ah.addAttribute(name, value);
		}

		template<typename T>
		void addAttribute(std::string name, std::vector<T> *value)
		{
			vec_attribute_helper< T> ah(subelements);
			ah.addAttribute(name, value);
		}

		template<typename T>
		void addAttribute(std::string name, T* value)
		{
			attribute_helper<T, std::is_base_of<abstract_element, T>::value> ah(subelements);
			ah.addAttribute(name, value);
		}

		template<typename T>
		void addAttribute(std::string name, std::shared_ptr<T> value)
		{
			attribute_helper<T, std::is_base_of<abstract_element, T>::value> ah(subelements);
			ah.addAttribute(name, value);
		}

		std::shared_ptr<abstract_element> getAttribute(std::string name)
		{
			if (hasAttribute(name))
				return subelements.at(name);
			return 0;
		}

		bool hasAttribute(std::string name)
		{
			return subelements.count(name) > 0;
		}
		
		model_element* get() { return this; }

		bool isClass() override { return true; }

		model_element *toModelElement() override { if (isClass()) { return this; } return nullptr; }

		abstract_element_type *toElementType() override {
			return nullptr;
		}
	};

	
	
	template<typename T, typename K>
	struct model_converter
	{
		static void is_valid_converter()
		{
			std::string msg1 = typeid(K).name() + std::string(" is not base of ") + typeid(T).name();
			std::string msg2 = typeid(K).name() + std::string(" is not base of ") + typeid(model_element).name();

			if (std::is_base_of<K, T>::value != true)
				throw std::runtime_error(msg1);
			else if (std::is_base_of<model_element, T>::value != true)
				throw std::runtime_error(msg2);
		}

		static std::vector< std::shared_ptr<T>> convert_vector(std::vector<K>& vec)
		{
			is_valid_converter();

			std::vector< std::shared_ptr<T>> ret;
			for (auto& k : vec)
			{
				ret.push_back(std::make_shared<T>(k));
			}

			return ret;
		}

		static std::vector< std::shared_ptr<T>> convert_vector(std::vector<K*>& vec)
		{
			is_valid_converter();

			std::vector< std::shared_ptr<T>> ret;
			for (auto k : vec)
			{
				ret.push_back(std::make_shared<T>(*k));
			}

			return ret;
		}

		static std::vector< std::shared_ptr<T>> convert_vector(std::vector<std::shared_ptr<K>>& vec)
		{
			is_valid_converter();

			std::vector< std::shared_ptr<T>> ret;
			for (auto k : vec)
			{
				ret.push_back(std::make_shared<T>(*k.get()));
			}

			return ret;
		}

		static std::shared_ptr<T> convert_model(std::shared_ptr<K>& old)
		{
			is_valid_converter();
			return std::make_shared<T>(*old);
		}

		static std::shared_ptr<T> convert_model(K& old)
		{
			is_valid_converter();
			return = std::make_shared<T>(old);
		}

		static std::shared_ptr<T> convert_model(K* old)
		{
			is_valid_converter();
			return std::make_shared<T>(*old);
		}
	};


}
#endif