#ifndef __HTTP_VIEW_H__
#define __HTTP_VIEW_H__
#include <fstream>
#include <string>
#include <sstream>
#include "model.h"
#include "html_parser/mini_xml.h"

#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/qi_parse_attr.hpp>
#include <boost/spirit/include/qi_parse_auto.hpp>

#include "html_parser/ast_parser.h"

#define VIEW_ROOT "htdocs/views/"

using boost::spirit::ascii::space;

namespace http_server
{
	class view
	{
		std::string _file;
		model _model;
		public:
			view(std::string file, model model) : _file(file), _model(model)
			{
				
			}
			~view(){}


			std::string parse()
			{
				std::ifstream ifs(VIEW_ROOT + _file + ".html");
				if (!ifs.is_open())
					return "";
				std::string storage = std::string((std::istreambuf_iterator<char>(ifs)),
					std::istreambuf_iterator<char>());

				return parse_markup(storage, _file);
			}

			std::string parse_markup(const std::string& input, const std::string& name);

			auto& getModel() { return _model; }

			const MyMarkupParser p;
			
	};
}


#endif