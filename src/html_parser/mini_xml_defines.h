#ifndef __MINI_XML_DEFINES_H__
#define __MINI_XML_DEFINES_H__

#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <memory>

#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS
#define BOOST_MPL_LIMIT_LIST_SIZE 30

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

namespace qi = boost::spirit::qi;
namespace phx = boost::phoenix;
namespace ascii = boost::spirit::ascii;

// AST node structs

struct ast_null;
struct ast_comment;
struct ast_nodelist;

struct ast_func_compare;
struct ast_func_variable;
struct ast_func_string;
struct ast_func_integer;
struct ast_func_double;
struct ast_func_call;
struct ast_func_include;
struct ast_func_filter;
struct ast_func_set;
struct ast_func_if;
struct ast_func_for;
struct ast_func_expr;
struct ast_func_template;

struct ast_tagged_node;
struct ast_html_node;
struct ast_html_selfnode;
struct ast_highlight;

typedef boost::variant<
	ast_null,
	std::string,
	ast_comment,
	boost::recursive_wrapper<ast_nodelist>,
	ast_func_variable,
	ast_func_string,
	ast_func_integer,
	ast_func_double,
	ast_func_template,
	//ast_func_compare,
	//boost::recursive_wrapper<ast_func_compare>,
	boost::recursive_wrapper<ast_func_variable>,
	boost::recursive_wrapper<ast_func_call>,
	boost::recursive_wrapper<ast_func_include>,
	boost::recursive_wrapper<ast_func_filter>,
	boost::recursive_wrapper<ast_func_set>,
	boost::recursive_wrapper<ast_func_if>,
	boost::recursive_wrapper<ast_func_for>,
	boost::recursive_wrapper<ast_func_expr>,
	boost::recursive_wrapper<ast_tagged_node>,
	boost::recursive_wrapper<ast_html_node>,
	boost::recursive_wrapper<ast_html_selfnode>,
	ast_highlight
>
ast_node;

// *** Individual AST node structs

//! represent null or undefined
struct ast_null
{
};

//! a comment <# clause #>
struct ast_comment : public std::string
{
};

//! a sequence of multiple AST nodes
struct ast_nodelist : public std::vector<ast_node>
{
};

//! MyFunc node representing a variable with subvariables
struct ast_func_variable
{
	std::string variable;
	ast_nodelist        args;

	ast_func_variable() {}

	ast_func_variable(const std::string& _varname)
		: variable(_varname)
	{
	}

	ast_func_variable(const std::string& _varname, const ast_nodelist& attr)
		: variable(_varname)
	{
		args = attr;
	}

};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_variable,
	(std::string, variable)
	(ast_nodelist, args)
)

//! MyFunc node representing a literal string
struct ast_func_string : public std::string
{
};

//! MyFunc node representing a template name
struct ast_func_template : public std::string
{
};

//! MyFunc node representing a literal integer
struct ast_func_integer
{
	long long   value;

	explicit inline ast_func_integer(const long long& v = 0)
		: value(v) {}
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_integer,
	(long long, value)
)

//! MyFunc node representing a literal double
struct ast_func_double
{
	double      value;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_double,
	(double, value)
)

//! tagged sequence of multiple AST nodes with HTML attributes, like <p [attr]> [nodes] </p>
struct ast_highlight
{
	std::string                 language;
	std::string                 content;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_highlight,
	(std::string, language)
	(std::string, content)
)

//! MyFunc node representing a function call with argument list
struct ast_func_call
{
	std::string         funcname;
	ast_nodelist        args;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_call,
	(std::string, funcname)
	(ast_nodelist, args)
)

struct ast_func_include
{
	std::string         include;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_include,
	(std::string, include)
)

struct ast_func_compare
{
	ast_node            left, right, comperator;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_compare,
	(ast_node, left)
	(ast_node, right)
	(ast_node, comperator)
)


//! MyFunc node representing a conditional clause
struct ast_func_filter
{
	ast_node            node;
	std::string         content;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_filter,
	(ast_node, node)
	(std::string, content)
)

//! MyFunc node representing a function call with argument list and filter content
struct ast_func_set
{
	std::string         varname;
	ast_node            value;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_set,
	(std::string, varname)
	(ast_node, value)
)

//! MyFunc node representing a function call with argument list and filter content
struct ast_func_if
{
	ast_node            condition, iftrue, iffalse;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_if,
	(ast_node, condition)
	(ast_node, iftrue)
	(ast_node, iffalse)
)

//! MyFunc node representing a function call with argument list and filter content
struct ast_func_for
{
	std::string         varname;
	ast_node            arg;
	ast_node            subtree;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_func_for,
	(std::string, varname)
	(ast_node, arg)
	(ast_node, subtree)
)

//! MyFunc node representing a sequence of expressions with operators intermingled
struct ast_func_expr : public ast_nodelist
{
};

//! tagged sequence of multiple AST nodes like <p> [nodes] </p>
struct ast_tagged_node
{
	std::string         tag;
	ast_node            subtree;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_tagged_node,
	(std::string, tag)
	(ast_node, subtree)
)

//! key-value attributes for HTML like name=value
struct ast_html_attr
{
	std::string         name;
	ast_node            value;
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_html_attr,
	(std::string, name)
	(ast_node, value)
)

//! a sequence of multiple key-value attributes for HTML
struct ast_html_attrlist : public std::vector<ast_html_attr>
{
	const ast_html_attr& find(const std::string& key) const
	{
		for (const_iterator it = begin(); it != end(); ++it)
		{
			if (it->name != key) continue;
			return *it;
		}
		std::cout << "{ERROR cannot find HTML attribute " << key << "}"
			<< std::endl;
		abort();
	}
};

//! tagged sequence of multiple AST nodes with HTML attributes, like <p [attr]> [nodes] </p>
struct ast_html_node
{
	std::string         tag;
	ast_html_attrlist   attrlist;
	ast_node            subtree;

	ast_html_node() {}

	ast_html_node(const std::string& _tag, const ast_html_attr& attr, const ast_node& _subtree)
		: tag(_tag), subtree(_subtree)
	{
		attrlist.push_back(attr);
	}
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_html_node,
	(std::string, tag)
	(ast_html_attrlist, attrlist)
	(ast_node, subtree)
)

//! tagged sequence of multiple AST nodes with HTML attributes, like <img [attr] />
struct ast_html_selfnode
{
	std::string         tag;
	ast_html_attrlist   attrlist;

	ast_html_selfnode() {}

	ast_html_selfnode(const std::string& _tag, const ast_html_attr& attr1)
		: tag(_tag)
	{
		attrlist.push_back(attr1);
	}

	ast_html_selfnode(const std::string& _tag, const ast_html_attr& attr1,
		const ast_html_attr& attr2)
		: tag(_tag)
	{
		attrlist.push_back(attr1);
		attrlist.push_back(attr2);
	}
};

BOOST_FUSION_ADAPT_STRUCT(
	ast_html_selfnode,
	(std::string, tag)
	(ast_html_attrlist, attrlist)
)

struct ast_debug : boost::static_visitor<>
{
	int depth;

	std::ostringstream oss;

	ast_debug(const ast_node& ast) : depth(0)
	{
		boost::apply_visitor(*this, ast);
	}

	inline std::string tab()
	{
		return std::string(2 * depth, ' ');
	}

	void operator()(const ast_null&)
	{
		oss << tab() << "NULL" << std::endl;
	}

	void operator()(const std::string& text)
	{
		oss << tab() << "text: \"" << text << '"' << std::endl;
	}

	void operator()(const ast_comment& text)
	{
		oss << tab() << "comment: \"" << text << '"' << std::endl;
	}

	void recurse(const ast_node& node)
	{
		++depth;
		boost::apply_visitor(*this, node);
		--depth;
	}

	void recurse_list(const ast_nodelist& nodelist)
	{
		oss << tab() << '{' << std::endl;

		for (const ast_node& n : nodelist)
			recurse(n);

		oss << tab() << '}' << std::endl;
	}

	void operator()(const ast_nodelist& ast)
	{
		recurse_list(ast);
	}

	void operator()(const ast_tagged_node& ast)
	{
		oss << tab() << '<' << ast.tag << '>' << std::endl;
		recurse(ast.subtree);
	}

	void operator()(const ast_html_node& ast)
	{
		oss << tab() << '<' << ast.tag << '>';
		if (ast.attrlist.size())
		{
			++depth;
			oss << " [" << std::endl;
			for (const ast_html_attr& attr : ast.attrlist)
			{
				oss << tab() << attr.name << '=' << std::endl;
				recurse(attr.value);
			}
			oss << tab() << ']';
			--depth;
		}
		oss << std::endl;

		recurse(ast.subtree);
	}

	void operator()(const ast_html_selfnode& ast)
	{
		oss << tab() << '<' << ast.tag << '>';
		if (ast.attrlist.size())
		{
			++depth;
			oss << " [" << std::endl;
			for (const ast_html_attr& attr : ast.attrlist)
			{
				oss << tab() << attr.name << '=' << std::endl;
				recurse(attr.value);
			}
			oss << tab() << ']';
			--depth;
		}
		oss << std::endl;
	}

	void operator()(const ast_func_variable& ast)
	{
		oss << tab() << "var: " << ast.variable << std::endl;
		oss << tab() << "[" << std::endl;
		recurse_list(ast.args);
		oss << tab() << "]" << std::endl;
	}

	void operator()(const ast_func_string& ast)
	{
		oss << tab() << "string: " << ast << std::endl;
	}

	void operator()(const ast_func_integer& ast)
	{
		oss << tab() << "integer: " << ast.value << std::endl;
	}

	void operator()(const ast_func_double& ast)
	{
		oss << tab() << "double: " << ast.value << std::endl;
	}

	void operator()(const ast_func_template& ast)
	{
		oss << tab() << "template: " << ast << std::endl;
	}

	void operator()(const ast_func_call& ast)
	{
		oss << tab() << "call: " << ast.funcname << " {" << std::endl;
		recurse_list(ast.args);
		oss << tab() << '}' << std::endl;
	}

	void operator()(const ast_func_filter& ast)
	{
		oss << tab() << "filter: [" << std::endl;
		recurse(ast.node);
		oss << tab() << "] on \"" << ast.content << "\"" << std::endl;
	}

	void operator()(const ast_func_expr& ast)
	{
		oss << tab() << "expr: {" << std::endl;
		recurse_list(ast);
		oss << tab() << '}' << std::endl;
	}

	void operator()(const ast_func_set& ast)
	{
		oss << tab() << "set: " << ast.varname << std::endl;
		oss << tab() << "value: " << std::endl;
		recurse(ast.value);
	}

	void operator()(const ast_func_if& ast)
	{
		oss << tab() << "if: [" << std::endl;
		recurse(ast.condition);
		oss << tab() << "]" << std::endl;
		oss << tab() << "true: " << std::endl;
		recurse(ast.iftrue);
		oss << tab() << "else: " << std::endl;
		recurse(ast.iffalse);
	}

	void operator()(const ast_func_include& ast)
	{
		return;
	}

	void operator()(const ast_func_compare& ast)
	{
		return;
	}

	void operator()(const ast_func_for& ast)
	{
		oss << tab() << "for: " << ast.varname << "[" << std::endl;
		recurse(ast.arg);
		oss << tab() << "]" << std::endl;
		oss << tab() << "subtree: " << std::endl;
		recurse(ast.subtree);
	}

	void operator()(const ast_highlight& ast)
	{
		oss << tab() << "highlight[" << ast.language << "]" << std::endl
			<< tab() << "\"" << ast.content << "\"" << std::endl;
	}
};

#endif