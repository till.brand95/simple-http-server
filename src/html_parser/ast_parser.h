#ifndef __AST_PARSER_H__
#define __AST_PARSER_H__

#include "mini_xml_defines.h"
#include "mvc/model.h"
#include <sstream>
#include <boost/any.hpp>
#include <boost/variant.hpp>
#include <tuple>
#include <limits>

#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/qi_parse_attr.hpp>
#include <boost/spirit/include/qi_parse_auto.hpp>

struct function_arg_parser : boost::static_visitor<>
{
	auto operator()(int i) const
	{
		return std::make_tuple(i);
	}

	auto operator()(const std::string& str) const
	{
		return std::make_tuple(str);
	}


};

struct ast_parser : boost::static_visitor<>
{
	int depth;

	std::ostringstream oss;
	http_server::model model;
	std::shared_ptr<http_server::abstract_function> current_function;

	std::shared_ptr < http_server::abstract_element> curr_element;

	std::string set_element;
	std::string for_element;

	bool set_function = false;
	bool if_function = false;
	bool if_function_result = false;
	bool function = false;
	bool for_function = false;
	int function_parameter_index = 0;

	std::shared_ptr<http_server::abstract_element> iterable_element;

	std::vector<std::string> parents;
		
	ast_parser(const ast_node& ast, http_server::model &_model) : depth(0), model(_model)
	{
		boost::apply_visitor(*this, ast);
	}

	inline std::string tab()
	{
		return std::string(2 * depth, ' ');
	}

	void operator()(const ast_null&)
	{
		oss << tab() << "NULL";
	}

	void operator()(const std::string& text)
	{
		oss << text;
	}

	void operator()(const ast_comment& text)
	{
		oss << "/*" <<  text << "*/";
	}

	void recurse(const ast_node& node)
	{
		++depth;
		boost::apply_visitor(*this, node);
		--depth;
	}

	void recurse_list(const ast_nodelist& nodelist)
	{
		for (const ast_node& n : nodelist)
			recurse(n);
	}

	void operator()(const ast_nodelist& ast)
	{
		recurse_list(ast);
	}

	void operator()(const ast_tagged_node& ast)
	{
		oss << tab() << '<' << ast.tag;
		if (ast.subtree.which() != 0)
		{	
			oss << '>' << std::endl;
			recurse(ast.subtree);
			oss << tab() << "</" << ast.tag << '>' << std::endl;
		}
		else
			oss << "/>" << std::endl;
		
	}

	void operator()(const ast_html_node& ast)
	{
		oss << tab() << '<' << ast.tag;
		if (ast.attrlist.size())
		{
			++depth;
			for (const ast_html_attr& attr : ast.attrlist)
			{
				oss << " " << attr.name << "=\"";
				recurse(attr.value);
				oss << "\"";
			}
			--depth;
		}
		if (ast.subtree.which() != 0)
		{
			oss << '>' << std::endl;
			recurse(ast.subtree);
			oss << tab() << "</" << ast.tag << '>' << std::endl;
		}
		else
			oss << "/>" << std::endl;
	}

	void operator()(const ast_html_selfnode& ast)
	{
		oss << tab() << '<' << ast.tag;
		if (ast.attrlist.size())
		{
			++depth;
			for (const ast_html_attr& attr : ast.attrlist)
			{
				oss << tab() << attr.name << "=\"";
				recurse(attr.value);
				oss << "\"";
			}
			--depth;
		}
		oss << "/>" << std::endl;
	}

	void operator()(const ast_func_variable& ast)
	{
		parents.push_back(ast.variable);
		auto size = parents.size();
		recurse_list(ast.args);
		
		if (parents.size() != size)
			return;

		auto val = model.getAttribute(parents.at(0));

		if (parents.size() > 1)
		{
			for (int i = 1; i < parents.size(); i++)
			{
				if (!val)
					return;
				auto xx = val->toModelElement();
				if (!xx)
					return;
				val = xx->getAttribute(parents.at(i));
			}
		}

		parents.clear();

		//set variable
		if (set_function) 
		{
			if (!val)
				return;
			
			model.addAttribute(set_element, val);
			 
			set_function = false;
		}
		else if (if_function)
		{
			if_function_result = val ? true : false;
			if_function = false;
		}
		else if (function)
		{
			if (!val)
				return;
			if (!current_function)
				return;
			if(val->isClass())
				current_function->setArgument(function_parameter_index++, val->toModelElement());
			else
				current_function->setArgument(function_parameter_index++, val->toElementType());
		}
		
		else // print
		{
			if (!val)
				return;
			if (!val->isClass())
			{
				oss << val->toElementType()->to_string();
			}
		}

		parents.clear();
			
	}

	void operator()(const ast_func_string& ast)
	{
		std::string val = ast;
		//set variable
		if (set_function)
		{
			
			
			model.addAttribute(set_element, val);
			
			set_function = false;
		}
		else if (if_function)
		{
			std::string str = ast;
			if_function_result = !str.empty();
			if_function = false;
		}
		else if (function)
		{
			current_function->setArgument(function_parameter_index++, val);
		}
		else // print
		{
			oss << ast;
		}
	}
	 
	void operator()(const ast_func_integer& ast)
	{
		//set variable
		int val = ast.value;
		if (set_function)
		{
			model.addAttribute(set_element, val);
			set_function = false;
		}
		else if (if_function)
		{
			int val = ast.value;
			if_function_result = val != 0;
			if_function = false;
		}
		else if (function)
		{
			current_function->setArgument(function_parameter_index++, val);
		}
		else // print
		{
			oss << ast.value;
		}
	}

	void operator()(const ast_func_double& ast)
	{
		double val = ast.value;
		//set variable
		if (set_function)
		{
			
			
			model.addAttribute(set_element, val);
			
			set_function = false;
		}
		else if (if_function)
		{
			double val = ast.value;
			if_function_result = val > 0 || val < 0;
			if_function = false;
		}
		else if (function)
		{
			current_function->setArgument(function_parameter_index++, val);
		}
		else // print
		{
			oss << ast.value;
		}
	}

	void operator()(const ast_func_template& ast)
	{
		oss << tab() << "template: " << ast << std::endl;
	}

	void operator()(const ast_func_call& ast)
	{
		function = true;
		auto size = parents.size();
		if (parents.size() > 0)
		{
			curr_element = model.getAttribute(parents.at(0));
			for (int i = 1; i < parents.size(); i++)
			{
				curr_element = curr_element->toModelElement()->getAttribute(parents.at(i));
			}
			current_function = curr_element->toModelElement()->getFunction(ast.funcname);
		}
		else
			current_function = model.getFunction(ast.funcname);

		if (!current_function)
			return;

		parents.clear();

		recurse_list(ast.args);		

		if (!current_function)
			return;
		
		function = false;
		auto ret = current_function->call();

		if (set_function)
		{
			if (!ret)
				return;

			model.addAttribute(set_element, ret);		

			set_function = false;
		}
		else if (if_function)
		{
			if_function_result = ret ? true : false;
			if_function = false;
		}
		else if (function)
		{
			if (!ret)
				return;
			if (ret->isClass())
				current_function->setArgument(function_parameter_index++, ret->toModelElement());
			else
				current_function->setArgument(function_parameter_index++, ret->toElementType());
		}

		else // print
		{
			if (!ret)
				return;
			if (!ret->isClass())
			{
				if(ret->toElementType()->iterable())
					oss << ret->toElementType()->asList()->to_string();
				else
					oss << ret->toElementType()->to_string();
			}
		}		
		
		parents.clear();
		function_parameter_index = 0;
	}

	void operator()(const ast_func_filter& ast)
	{
		oss << tab() << "filter: [" << std::endl;
		recurse(ast.node);
		oss << tab() << "] on \"" << ast.content << "\"" << std::endl;
	}

	void operator()(const ast_func_expr& ast)
	{
		recurse_list(ast);
	}

	void operator()(const ast_func_set& ast)
	{
		set_element = ast.varname;
		set_function = true;
		recurse(ast.value);
	}

	void operator()(const ast_func_if& ast)
	{
		if_function = true;
		recurse(ast.condition);
		if (if_function_result)
		{
			recurse(ast.iftrue);
			if_function_result = false;
		}
		else
		{
			recurse(ast.iffalse);
			if_function_result = false;
		}

	}

	void operator()(const ast_func_for& ast)
	{
		set_element = "iterable_element";
		set_function = true;
		recurse(ast.arg);
		for_element = ast.varname;
		
		auto attr = model.getAttribute(set_element)->toElementType();
		if (attr)
		{
			if (!attr->iterable())
				return;
			auto vector = attr->asList();
			for (int i = 0; i < vector->size(); i++)
			{
				auto curr_attr = vector->get(i);
				model.addAttribute(ast.varname, curr_attr);
				recurse(ast.subtree);
			}
		}	
		model.removeAttribute(ast.varname);
		model.removeAttribute(set_element);

	}

	void operator()(const ast_func_include& ast)
	{
		std::ifstream ifs(std::string("htdocs/views/") + ast.include);
		if (!ifs.is_open())
			return;
		std::string input = std::string((std::istreambuf_iterator<char>(ifs)),
			std::istreambuf_iterator<char>());
		std::string::const_iterator
			begin = input.begin(), end = input.end();
		ast_node astn;
		MyMarkupParser p;
		bool r = boost::spirit::qi::phrase_parse(begin, end, p, qi::space, astn);
		if(r)
			recurse(astn);
	}

	void operator()(const ast_func_compare& ast)
	{
		return;
	}

	void operator()(const ast_highlight& ast)
	{
		oss << tab() << "highlight[" << ast.language << "]" << std::endl
			<< tab() << "\"" << ast.content << "\"" << std::endl;
	}

	
};

#endif