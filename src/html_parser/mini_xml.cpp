#include "mini_xml.h"


MyMarkupParser::MyMarkupParser() : base_type(Start, "MyMarkupParser")
{
	using namespace boost::spirit::ascii;
	using namespace qi::labels;

	using qi::lit;
	using qi::eoi;
	using qi::eol;
	using qi::attr;
	using qi::omit;
	using qi::as_string;

	// ********************************************************************
	// *** General Base Character Parsers

	// text is composed of non-special characters
	HtmlText = +(char_("A-Za-z0-9~@$^.,:;_=+({}|?/-")[_val += _1]
		| lit('&')[_val += "&amp;"]
		| lit('"')[_val += "&quot;"]
		| lit('\'')[_val += "&apos;"]
		| lit('>')[_val += "&gt;"]
		| lit('\304')[_val += "&Auml;"]
		| lit('\326')[_val += "&Ouml;"]
		| lit('\334')[_val += "&Uuml;"]
		| lit('\337')[_val += "&szlig;"]
		| lit('\344')[_val += "&auml;"]
		| lit('\350')[_val += "&egrave;"]
		| lit('\351')[_val += "&eacute;"]
		| lit('\366')[_val += "&ouml;"]
		| lit('\374')[_val += "&uuml;"]
		| (+blank >> -(eol >> *blank >> !eol))[_val += " "]
		| (eol >> *blank >> !eol)[_val += " "]
		);

	// special characters, accepted if no special meaning
	SpecialChar = (char_("*`#[])!")[_val += _1]
		//| (lit('<') >> !lit("%")) [ _val += "&lt;" ]
		| lit("\\\\")[_val += '\\']
		| lit("\\\"")[_val += '"']
		| lit("\\&")[_val += '&']
		| lit("\\�")[_val += '�']
		| lit("\\@")[_val += '@']
		| lit("\\*")[_val += '*']
		| lit("\\#")[_val += '#']
		| lit("\\`")[_val += '`']
		| lit("\\[")[_val += '[']
		| lit("\\<")[_val += "&lt;"]
		| (lit('%') >> !lit('%'))[_val += '%']
		);

	// a blank file
	BlankLine = *blank >> eol;

	// identation for lists
	Indent = lit('\t') | lit("  ");

	// ********************************************************************
	// *** Inline Blocks with Special Formatting

	Inline %= Comment | VerbatimInline | FilterInline | FuncInline | HtmlPhrase | HtmlText | SpecialChar | Doctype;

	InlinePlain %= Comment | VerbatimInline | FilterInline | FuncInline | PlainText;

	// inline comments

	Comment %= "<%#" >> *(!lit("%>") >> char_) >> "%>";

	CommentBlock %= "<%#" >> *(!lit("%>") >> char_) >> "%>" >> omit[*eol];

	// inline functional language

	FuncBlock %= "<%" >> qi::skip(qi::space)[FClause] >> omit[*space] >> "%>" >> omit[eol];

	FuncInline %= "<%" >> qi::skip(qi::space)[FClause] >> omit[*space] >> "%>";

	FilterBlock %= "<%|" >> qi::skip(qi::space)[FFilterClause] >> omit[*space] >> "%>" >> omit[eol]
		>> *(!(eol >> "<%|%>") >> char_)
		>> omit[eol] >> "<%|%>";

	FilterInline %= "<%|" >> qi::skip(qi::space)[FFilterClause] >> omit[*space] >> "%>" >> omit[-eol]
		>> *(!(-eol >> "<%|%>") >> char_)
		>> omit[-eol] >> "<%|%>";

	VerbatimBlock %= "<%$" >> omit[eol] >> *(!lit("%>") >> char_) >> "%>" >> omit[eol];

	VerbatimInline %= "<%$" >> *(!lit("%>") >> char_) >> "%>";

	// ********************************************************************
	// *** Inline HTML blocks

	HtmlSelfCloseTagName =
		string("meta") |
		string("link") |
		string("input") |
		string("br") |
		string("img") |
		string("hr");

	HtmlTagName =
		string("link") |
		string("label") |
		string("input") |
		string("big") |
		string("br") |
		string("button") |
		string("caption") |
		string("code") |
		string("col") |
		string("dd") |
		string("div") |
		string("dl") |
		string("dt") |
		string("em") |
		string("footer") |
		string("form") |
		string("h1") | string("h2") | string("h3") | string("h4") | string("h5") | string("h6") |
		string("hr") |
		string("i") |
		string("iframe") |
		string("img") |
		string("li") |
		string("longversion") |
		string("meta") |
		string("nav") |
		string("object") |
		string("ol") |
		string("option") |
		string("param") |
		string("pre") |
		string("select") |
		string("script") |
		string("span") |
		string("strong") |
		string("sup") |
		string("title") |
		string("table") |
		string("tbody") |
		string("td") |
		string("textarea") |
		string("tfoot") |
		string("thead") |
		string("tr") |
		string("html") |
		string("head") |
		string("body") |
		string("tt") |
		string("ul") |
		// two letter overlap
		string("th") |
		// one letter matches
		string("a") | string("b") | string("i") | string("p")
		;

	HtmlPhrase %= &lit('<') >> (HtmlComment | HtmlTagSelfClose | HtmlTagBlock);

	HtmlTagBlock %= '<' >> HtmlTagName[phx::at_c<0>(_val) = qi::_1]
		>> *HtmlAttribute >> omit[*space] >> '>' >> omit[*eol]
		>> HtmlInline
		>> omit["</" > string(phx::at_c<0>(_val)) >> '>']
		>> omit[*eol];

	HtmlInline %= *(Inline >> omit[*eol]);

	HtmlTagSelfClose %= ('<' >> HtmlSelfCloseTagName
		>> *HtmlAttribute >> omit[*space] >> ">"
		>> omit[*eol]) | ('<' >> HtmlTagName
		>> *HtmlAttribute >> omit[*space] >> "/>"
		>> omit[*eol]);

	DoctypeName = string("!DOCTYPE html");

	Doctype %= '<' >> DoctypeName >> *HtmlAttribute >> omit[*space] >> ">" >> omit[*eol];

	HtmlComment %= string("<!--") >> *(!lit("-->") >> char_) >> string("-->") >> omit[*eol];

	HtmlAttribute %= omit[+space] >> +(alnum | char_('-')) >> omit[*space >> '=' >> *space] >> HtmlQuoted;

	HtmlQuotedText = +(char_("A-Za-z0-9~!@#$%^.,:;_=+*()[]{}>'|?/ -")[_val += _1]
		| (lit('<') >> !lit('%'))[_val += '<']
		| lit('&')[_val += "&amp;"]
		| lit('�')[_val += "&euro;"]
		| lit('\304')[_val += "&Auml;"]
		| lit('\326')[_val += "&Ouml;"]
		| lit('\334')[_val += "&Uuml;"]
		| lit('\337')[_val += "&szlig;"]
		| lit('\344')[_val += "&auml;"]
		| lit('\350')[_val += "&egrave;"]
		| lit('\351')[_val += "&eacute;"]
		| lit('\366')[_val += "&ouml;"]
		| lit('\374')[_val += "&uuml;"]
		| lit("\\\"")[_val += '"']
		);

	HtmlQuoted %= '"' >> *(!lit('"') >> (Comment | FuncInline | HtmlQuotedText)) >> '"';

	qi::on_error<qi::fail>(
		HtmlTagBlock,
		std::cout << phx::val("{debug error expecting ") << _4 << phx::val(" here: \"")
		<< phx::construct<std::string>(_3, _2)   // iterators to error-pos, end
		<< phx::val("\"}") << std::endl
		);

	// ********************************************************************
	// *** Paragraph Blocks: Enumerations

	Bullet = char_("+*-") >> +blank;
	Enumet = +digit >> '.' >> +blank;

	BulletList0 %= &Bullet >> attr("ul") >> List0;
	OrderedList0 %= &Enumet >> attr("ol") >> List0;

	BulletList1 %= &(Indent >> Bullet) >> attr("ul") >> List1;
	OrderedList1 %= &(Indent >> Enumet) >> attr("ol") >> List1;

	BulletList2 %= &(Indent >> Indent >> Bullet) >> attr("ul") >> List2;
	OrderedList2 %= &(Indent >> Indent >> Enumet) >> attr("ol") >> List2;

	List0 %= +ListItem0;
	List1 %= +ListItem1;
	List2 %= +ListItem2;

	ListItem0 %= omit[(Bullet | Enumet)] >> attr("li") >> ListBlock0;
	ListItem1 %= omit[Indent >> (Bullet | Enumet)] >> attr("li") >> ListBlock1;
	ListItem2 %= omit[Indent >> Indent >> (Bullet | Enumet)] >> attr("li") >> ListBlock2;

	ListBlock0 %= !BlankLine >> Line >> *(BulletList1 | OrderedList1 | ListBlockLine0);
	ListBlock1 %= !BlankLine >> Line >> *(BulletList2 | OrderedList2 | ListBlockLine1);
	ListBlock2 %= !BlankLine >> Line >> *(ListBlockLine2);

	ListBlockLine0 %= !BlankLine >> !(*Indent >> (Bullet | Enumet))
		>> Indent >> attr(" ") >> Line;

	ListBlockLine1 %= !BlankLine >> !(*Indent >> (Bullet | Enumet))
		>> Indent >> Indent >> attr(" ") >> Line;

	ListBlockLine2 %= !BlankLine >> !(*Indent >> (Bullet | Enumet))
		>> Indent >> Indent >> Indent >> attr(" ") >> Line;

	// inline will gobble single eols, but stop at double eols.
	Line %= +Inline >> omit[(eol >> BlankLine) | (*eol >> eoi)];

	// ********************************************************************
	// *** Paragraph Blocks: Headers

	Header6 %= "###### " >> attr("h6") >> InlineList;
	Header5 %= "##### " >> attr("h5") >> InlineList;
	Header4 %= "#### " >> attr("h4") >> InlineList;
	Header3 %= "### " >> attr("h3") >> InlineList;
	Header2 %= "## " >> attr("h2") >> InlineList;
	Header1 %= "# " >> attr("h1") >> InlineList;

	HeaderAnchor = as_string[+~char_(')')]
		[_val = "<a id=\"" + _1 + "\"></a>"];

	HeaderA %= HeaderAnchor >> lit(") ") >> InlineList;

	Header6A %= "######(" >> attr("h6") >> HeaderA;
	Header5A %= "#####(" >> attr("h5") >> HeaderA;
	Header4A %= "####(" >> attr("h4") >> HeaderA;
	Header3A %= "###(" >> attr("h3") >> HeaderA;
	Header2A %= "##(" >> attr("h2") >> HeaderA;
	Header1A %= "#(" >> attr("h1") >> HeaderA;

	Header %= &lit('#') >> (Header6A | Header5A | Header4A | Header3A | Header2A | Header1A |
		Header6 | Header5 | Header4 | Header3 | Header2 | Header1);

	// ********************************************************************
	// *** Source Highlighting Code Blocks

	HighlightBlock %= "```" >> omit[*blank] >> *print >> omit[eol]
		>> *(!(eol >> "```") >> char_)
		>> omit[eol] >> "```" >> omit[*blank >> eol];

	// ********************************************************************
	// *** Paragraph Blocks: Paragraphs and Plain

	Paragraph %= attr("p") >> ParagraphBlock >> omit[+(eol | blank >> eoi)];
	ParagraphBlock %= InlineList;

	InlineList %= +Inline;

	Block %= omit[*BlankLine] >> (
		CommentBlock |
		VerbatimBlock | FilterBlock | FuncBlock |
		HighlightBlock |
		Header |
		BulletList0 | OrderedList0 |
		HtmlPhrase |
		Paragraph | InlineList);

	BlockList %= *Block;

	Start %= BlockList;

	// ********************************************************************
	// *** Inline Procedural Language

	Operator = string("<") | string(">") | string("==") | string("!=") | string(">=") | string("<=") | string("=>") | string("=<");

	FIdentifier %= char_("A-Za-z_") >> *char_("A-Za-z0-9_");

	FVariable %= FIdentifier >> -('.' >> FExpr);

	FString %= '"' >> *(!lit('"') >> ((lit("\\\"") >> attr('"')) | char_)) >> '"';

	FDouble %= qi::real_parser< double, qi::strict_real_policies<double> >();

	FInteger %= qi::long_long;

	FCall %= FIdentifier >> '(' >> -(FExpr % ',') >> ')';

	FBracket %= '(' >> FExpr >> ')';

	FAtomic %= FBracket | FCall | FString | FDouble | FInteger | FVariable;

	FExpr %= FAtomic % as_string[char_("+")][phx::push_back(_val, _1)];

	//FCompare %= FVariable >> Operator >> FVariable;

	FSetClause %= -lit("SET") >> FIdentifier >> '=' >> FExpr;

	FIfClause %= "IF" >> FExpr >> "%%" >> Start >> "%%"
		>> -(lit("ELSE") >> "%%" >> Start >> "%%")
		>> "ENDIF";

	FEvalIfClause %= "EVALIF" >> FExpr >> "%%" >> FClause >> "%%"
		>> -(lit("ELSE") >> "%%" >> FClause >> "%%")
		>> "ENDIF";

	FForClause %= "FOR" >> FIdentifier >> ':' >> FExpr >> "%%" >> Start >> "%%"
		>> "ENDFOR";

	IncludeFile %= char_("A-Za-z0-9~@$^.,_=+-/\\") >> *char_("A-Za-z0-9~@$^.,_=+-/\\");

	FInclude %= -lit("INCLUDE") >> "\"" >> IncludeFile >> "\"";
	FClause %= FSetClause | FEvalIfClause | FIfClause | FForClause | FInclude | FExpr;
	
	FFilterSetClause %= "SET" >> FIdentifier >> -('.' >> FExpr);

	FFilterTemplateClause %= "TEMPLATE" >> FIdentifier;

	FFilterClause %= FFilterSetClause | FFilterTemplateClause | FCall ;

	// ********************************************************************
}

