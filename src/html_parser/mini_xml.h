// Example how to use Boost Spirit to parse a HTML-like markup language with
// Markdown elements and enable additional instructions. This example was
// extracted from a HTML template engine, but only the AST printer is included.
//
// This example is designed to read "example.html".

#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <memory>

#include "mini_xml_defines.h"

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

namespace qi = boost::spirit::qi;
namespace phx = boost::phoenix;
namespace ascii = boost::spirit::ascii;



/******************************************************************************/
// MyMarkup parser

struct MyMarkupParser : qi::grammar<std::string::const_iterator, ast_node()>
{
	typedef std::string::const_iterator Iterator;

	// *** General Base Character Parsers

	qi::rule<Iterator, std::string()> HtmlText, SpecialChar, PlainText, IncludeFile;

	qi::rule<Iterator> BlankLine, Indent;

	// *** Inline Blocks with Special Formatting

	qi::rule<Iterator, ast_node()> Inline, InlinePlain;

	qi::rule<Iterator, ast_comment()> Comment, CommentBlock;

	qi::rule<Iterator, ast_tagged_node()> Code, Emph, Strong;
	qi::rule<Iterator, ast_nodelist()> CodeBlock, EmphBlock, StrongBlock;

	qi::rule<Iterator, ast_html_node()> MarkLink;
	qi::rule<Iterator, ast_nodelist()> MarkLinkText, MarkLinkRefList;
	qi::rule<Iterator, ast_html_attr()> MarkLinkRef;

	qi::rule<Iterator, ast_html_selfnode()> MarkImage;
	qi::rule<Iterator, ast_html_attr()> MarkImageAlt, MarkImageSrc;

	qi::rule<Iterator, ast_html_selfnode()> MarkDownload;
	qi::rule<Iterator, ast_html_attr()> MarkDownloadRef;

	qi::rule<Iterator, std::string()> HttpLink, SelfLink;

	qi::rule<Iterator, ast_node()> FuncBlock, FuncInline;
	qi::rule<Iterator, ast_func_filter()> FilterBlock, FilterInline;
	qi::rule<Iterator, std::string()> VerbatimBlock, VerbatimInline;

	// *** Inline HTML blocks

	qi::rule<Iterator, std::string()> HtmlTagName, HtmlComment, HtmlSelfCloseTagName, DoctypeName, Operator;

	qi::rule<Iterator, ast_node()> HtmlPhrase;
	qi::rule<Iterator, ast_html_node()> HtmlTagBlock;
	qi::rule<Iterator, ast_html_selfnode()> HtmlTagSelfClose, Doctype;
	qi::rule<Iterator, ast_nodelist()> HtmlInline;

	qi::rule<Iterator, ast_html_attr()> HtmlAttribute;
	qi::rule<Iterator, ast_nodelist()> HtmlQuoted;
	qi::rule<Iterator, std::string()> HtmlQuotedText;

	// *** Paragraph Blocks: Enumerations

	qi::rule<Iterator> Bullet, Enumet;

	qi::rule<Iterator, ast_tagged_node()> BulletList0, BulletList1, BulletList2;
	qi::rule<Iterator, ast_tagged_node()> OrderedList0, OrderedList1, OrderedList2;

	qi::rule<Iterator, ast_nodelist()> List0, List1, List2;

	qi::rule<Iterator, ast_tagged_node()> ListItem0, ListItem1, ListItem2;

	qi::rule<Iterator, ast_nodelist()> ListBlock0, ListBlock1, ListBlock2;
	qi::rule<Iterator, ast_nodelist()> ListBlockLine0, ListBlockLine1, ListBlockLine2;

	qi::rule<Iterator, ast_nodelist()> Line;

	// *** Paragraph Blocks: Headers

	qi::rule<Iterator, ast_tagged_node()> Header, Header1, Header2, Header3, Header4, Header5, Header6;

	qi::rule<Iterator, ast_nodelist()> HeaderA;
	qi::rule<Iterator, std::string()> HeaderAnchor;

	qi::rule<Iterator, ast_tagged_node()> Header1A, Header2A, Header3A, Header4A, Header5A, Header6A;

	// *** Source Highlighting Code Blocks

	qi::rule<Iterator, ast_highlight()> HighlightBlock;

	// *** Paragraph Blocks: Paragraphs and Plain

	qi::rule<Iterator, ast_tagged_node()> Paragraph;
	qi::rule<Iterator, ast_nodelist()> ParagraphBlock;

	qi::rule<Iterator, ast_nodelist()> InlineList;

	qi::rule<Iterator, ast_node()> Block;

	qi::rule<Iterator, ast_nodelist()> BlockList;

	qi::rule<Iterator, ast_node()> Start;

	// *** Inline Procedural Language

	typedef qi::space_type Skip;

	qi::rule<Iterator, std::string()> FIdentifier;
	qi::rule<Iterator, ast_func_variable(), Skip> FVariable;
	qi::rule<Iterator, ast_func_string()> FString;
	qi::rule<Iterator, ast_func_double(), Skip> FDouble;
	qi::rule<Iterator, ast_func_integer(), Skip> FInteger;
	qi::rule<Iterator, ast_func_call(), Skip> FCall;
	qi::rule<Iterator, ast_node(), Skip> FBracket;

	//qi::rule<Iterator, ast_func_compare(), Skip> FCompare;

	qi::rule<Iterator, ast_node(), Skip> FAtomic;
	qi::rule<Iterator, ast_func_expr(), Skip> FExpr;

	qi::rule<Iterator, ast_func_set(), Skip> FSetClause;
	qi::rule<Iterator, ast_func_if(), Skip> FIfClause, FEvalIfClause;
	qi::rule<Iterator, ast_func_for(), Skip> FForClause;
	qi::rule<Iterator, ast_func_include(), Skip> FInclude;

	qi::rule<Iterator, ast_node(), Skip> FClause;

	qi::rule<Iterator, ast_func_variable(), Skip> FFilterSetClause;
	qi::rule<Iterator, ast_func_template(), Skip> FFilterTemplateClause;
	qi::rule<Iterator, ast_node(), Skip> FFilterClause;

	// *** Construction

	MyMarkupParser();

	static const MyMarkupParser& get(); // get singleton
};