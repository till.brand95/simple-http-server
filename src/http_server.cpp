#include "http_server.h"

#include <boost/filesystem.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/archive/iterators/insert_linebreaks.hpp>
#include <boost/archive/iterators/remove_whitespace.hpp>
#include <algorithm>    

#include "mvc/user_repository.h"

#include "mvc/login_controller.h"

void http_worker::accept()
{
	// Clean up any previous connection.
	try {
		beast::error_code ec;
		socket_.close(ec);
		buffer_.consume(buffer_.size());

		acceptor_.async_accept(
			socket_,
			[this](beast::error_code ec)
			{
				if (ec)
				{
					accept();
				}
				else
				{
					// Request must be fully processed within 60 seconds.
					request_deadline_.expires_after(
						std::chrono::seconds(60));

					read_request();
				}
			});
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}

void http_worker::read_request()
{
	// On each read the parser needs to be destroyed and
	// recreated. We store it in a boost::optional to
	// achieve that.
	//
	// Arguments passed to the parser constructor are
	// forwarded to the message object. A single argument
	// is forwarded to the body constructor.
	//
	// We construct the dynamic body with a 1MB limit
	// to prevent vulnerability to buffer attacks.
	//
	try {
		parser_.emplace(
			std::piecewise_construct,
			std::make_tuple(),
			std::make_tuple(alloc_));

		http::async_read(
			socket_,
			buffer_,
			*parser_,
			[this](beast::error_code ec, std::size_t)
			{
				if (ec)
					accept();
				else
					process_request(parser_->get());
			});
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}

void http_worker::read_body()
{
	// On each read the parser needs to be destroyed and
	// recreated. We store it in a boost::optional to
	// achieve that.
	//
	// Arguments passed to the parser constructor are
	// forwarded to the message object. A single argument
	// is forwarded to the body constructor.
	//
	// We construct the dynamic body with a 1MB limit
	// to prevent vulnerability to buffer attacks.
	//
	try {
		parser_.emplace(
			std::piecewise_construct,
			std::make_tuple(),
			std::make_tuple(alloc_));

		http::async_read(
			socket_,
			buffer_,
			*parser_,
			[this](beast::error_code ec, std::size_t)
			{
				/*if (ec)
					accept();
				else
					process_request(parser_->get());*/
			});
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}

std::string decode(std::string input)
{
	using namespace boost::archive::iterators;
	typedef transform_width<binary_from_base64<remove_whitespace
		<std::string::const_iterator> >, 8, 6> ItBinaryT;

	try
	{
		// If the input isn't a multiple of 4, pad with =
		size_t num_pad_chars((4 - input.size() % 4) % 4);
		input.append(num_pad_chars, '=');

		size_t pad_chars(std::count(input.begin(), input.end(), '='));
		std::replace(input.begin(), input.end(), '=', 'A');
		std::string output(ItBinaryT(input.begin()), ItBinaryT(input.end()));
		output.erase(output.end() - pad_chars, output.end());
		return output;
	}
	catch (std::exception const&)
	{
		return std::string("");
	}
}

void http_worker::process_request(http::request<request_body_t, http::basic_fields<alloc_t>> const& req)
{
	for (auto& field : req)
	{
		std::cout << field.name() << ":" << field.value() << std::endl;
	}
	std::string site_name = req.target().to_string();
	if (site_name.find("static") != std::string::npos)
	{
		send_file(site_name);
		return;
	}
	auto callback = http_server::controller::getCallback(site_name, req.method());

	if (!callback.callback)
	{
		callback = http_server::controller::getCallback("/404", HTTP_GET);
		if (callback.callback)
		{
			
			http_server::model me = http_server::model();
			auto v = callback(&me);
			if (v == "")
			{
				send_bad_response(
					http::status::not_found,
					"404 - Not Found\r\n");
				return;
			}
			http_server::view view(v, me);
			
			send_view(view);

			return;

		}
		else
		{
			send_bad_response(
				http::status::not_found,
				"404 - Not Found\r\n");
			return;
		}
	}

	bool auth = false;
	std::string sessionId = "";
	http_server::model me = http_server::model();
	std::cout << "Header: " << std::endl;
	for (auto& field : req)
	{
		if (field.name() == http::field::cookie)
		{
			auto val = field.value();
			auto pos = val.find('=');
			auto name = val.substr(0, pos);
			if (name == "sessionId")
			{
				sessionId = val.substr(pos + 1, val.size() - 1 - pos).to_string();
				if (login_->in_use())
				{
					auth = login_->isLoggedIn(sessionId);
					if(auth)
						me.addAttribute("authenticated_user", login_->getUserByToken(sessionId));
				}
			}
		}
	}
	if (!auth && !	sessionId.empty())
	{
		send_redirect(site_name, true);
		return;
	}
	if (callback.authentificate && !auth && login_->in_use())
	{
		send_redirect(login_->getLoginpage());
		return;
	}
	
	std::string view_name = "";
	std::stringstream ss(boost::beast::buffers_to_string(req.body().data()));
	std::string var;
	std::vector<std::string> elems;
	switch (req.method())
	{
	case HTTP_GET:
			
		if(callback.callback)
			view_name = callback(&me);
		break;
	case HTTP_POST:
		if (callback.callback)
		{
			while (std::getline(ss, var, '&')) {

				me.addAttribute(var.substr(0, var.find_first_of('=')), var.substr(var.find_first_of('=') + 1, var.size() - 1));
			}
			view_name = callback(&me);
			
		} 		
		
		break;
	default:
		send_bad_response(
			http::status::bad_request,
			"Invalid request-method '" + std::string(req.method_string()) + "'\r\n");
		return;
	}
	if (view_name == "")
	{
		send_bad_response(
			http::status::bad_request,
			"Invalid request-method '" + std::string(req.method_string()) + "'\r\n");
		return;
	}

	if (me.hasAttribute("sessionId") && req.method() == HTTP_POST)
	{
		try {
			view_name = view_name.substr(view_name.find(":") + 1, view_name.size() - 1);
			while (view_name != "" && view_name[0] == ' ')
				view_name = view_name.substr(1, view_name.size() - 2);
			if (view_name[0] != '/')
				view_name = '/' + view_name;
			auto sc = set_cookies("sessionId", me.getAttribute("sessionId")->to_string());
			me.removeAttribute("sessionId");
			send_redirect(view_name, sc);
			
		}
		catch (std::exception e)
		{
			std::cout << e.what() << std::endl;
		}
		return;
	}

	if (view_name.find("redirect:") != std::string::npos)
	{
		try
		{
			view_name = view_name.substr(view_name.find(":") + 1, view_name.size() - 1);
			while (view_name != "" && view_name[0] == ' ')
				view_name = view_name.substr(1, view_name.size() - 2);
			if (view_name[0] != '/')
				view_name = '/' + view_name;
			
			send_redirect(view_name);
			
		}
		catch (std::exception e)
		{
			std::cout << "Exception happend on writing response to " << req.method() << ", with View: " << view_name << " Error:" << e.what() << std::endl;
		}
		return;
	}
	
	try
	{
		http_server::view vm(view_name, me);
		send_view(vm);
		
	}
	catch (std::exception e)
	{
		std::cout << "Exception happend on writing response to " << req.method() << ", with View: " << view_name << " Error:" << e.what() << std::endl;
	}
	
}

void http_worker::send_redirect(std::string to, set_cookies sc)
{
	while (empty_response_.has_value())
	{
	}
	try {
		empty_response_.emplace(http::response<http::empty_body>{});
		empty_response_->result(http::status::see_other);
		empty_response_->version(11);
		empty_response_->set(http::field::server, "Beast");
		empty_response_->set(http::field::location, to);
		if (sc.values.size() > 0)
		{
			std::string cookiestr = "";
			int i = 0;
			for (auto& val : sc.values)
			{
				i = i + 1;
				cookiestr += val.first + "=" + val.second;
				if (i != sc.values.size())
					cookiestr += "; ";
			}
			empty_response_->set(http::field::set_cookie, cookiestr);
		}
		
		empty_response_->prepare_payload();

		while (empty_serializer_.has_value())
		{
		}
		empty_serializer_.emplace(*empty_response_);

		http::async_write(
			socket_,
			*empty_serializer_,
			[this](beast::error_code ec, std::size_t)
			{
				socket_.shutdown(tcp::socket::shutdown_send, ec);
				empty_serializer_.reset();
				empty_response_.reset();
				accept();
			});
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}

void http_worker::send_redirect(std::string to, bool delete_cookies)
{
	while (empty_response_.has_value())
	{
	}
	try {
		empty_response_.emplace(http::response<http::empty_body>{});
		empty_response_->result(http::status::see_other);
		empty_response_->version(11);
		empty_response_->set(http::field::server, "Beast");
		empty_response_->set(http::field::location, to);
		if (delete_cookies)
		{
			empty_response_->set(http::field::set_cookie, "sessionId=deleted; expires=Thu, 01 Jan 1970 00:00:00 GMT");
		}
		empty_response_->prepare_payload();

		while (empty_serializer_.has_value())
		{
		}
		empty_serializer_.emplace(*empty_response_);

		http::async_write(
			socket_,
			*empty_serializer_,
			[this](beast::error_code ec, std::size_t)
			{
				socket_.shutdown(tcp::socket::shutdown_send, ec);
				empty_serializer_.reset();
				empty_response_.reset();
				accept();
			});
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}

void http_worker::send_view(http_server::view& vm)
{
	while (string_response_.has_value())
	{
	}
	try {
		string_response_.emplace(http::response<http::string_body>{});

		string_response_->result(http::status::ok);
		string_response_->version(11);
		string_response_->set(http::field::server, "Beast");
		string_response_->set(http::field::content_type, "text/html");
		string_response_->body() = vm.parse();
		string_response_->prepare_payload();

		while (string_serializer_.has_value())
		{
		}
		string_serializer_.emplace(*string_response_);

		http::async_write(
			socket_,
			*string_serializer_,
			[this](beast::error_code ec, std::size_t)
			{
				socket_.shutdown(tcp::socket::shutdown_send, ec);
				string_serializer_.reset();
				string_response_.reset();
				accept();
			});
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}


void http_worker::send_bad_response(http::status status, std::string const& error)
{
	while(string_response_.has_value())
	{ }
	try{
	string_response_.emplace(http::response<http::empty_body>{});

	string_response_->result(status);
	string_response_->keep_alive(false);
	string_response_->set(http::field::server, "Beast");
	string_response_->set(http::field::content_type, "text/plain");
	string_response_->body() = error;
	string_response_->prepare_payload();

	while(string_serializer_.has_value())
	{ }
	string_serializer_.emplace(*string_response_);

	http::async_write(
		socket_,
		*string_serializer_,
		[this](beast::error_code ec, std::size_t)
		{
			socket_.shutdown(tcp::socket::shutdown_send, ec);
			string_serializer_.reset();
			string_response_.reset();
			accept();
		});
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}

void http_worker::send_file(beast::string_view target)
{
	// Request path must be absolute and not contain "..".
	if (target.empty() || target[0] != '/' || target.find("..") != std::string::npos)
	{
		send_bad_response(
			http::status::not_found,
			"File not found\r\n");
		return;
	}
	auto mtype = mime_type(std::string(target));
	if (mtype == "")
	{
		send_bad_response(
			http::status::not_found,
			"Unrecognized meme type\r\n");
		return;
	}

	std::string full_path = doc_root_;
	full_path.append(
		target.data(),
		target.size());

	http::file_body::value_type file;
	beast::error_code ec;
	file.open(
		full_path.c_str(),
		beast::file_mode::read,
		ec);

	

	if (ec )
	{
		send_bad_response(
			http::status::not_found,
			"File not found\r\n");
		return;
	}

	
	while (file_response_.has_value())
	{
	}
	try
	{
		file_response_.emplace(http::response<http::file_body>{});
	
		file_response_->result(http::status::ok);
		file_response_->keep_alive(false);
		file_response_->set(http::field::server, "Beast");
		file_response_->set(http::field::content_type, mtype);
		file_response_->body() = std::move(file);
		file_response_->prepare_payload();

		while (file_serializer_.has_value())
		{
		}
		file_serializer_.emplace(*file_response_);

		http::async_write(
			socket_,
			*file_serializer_,
			[this](beast::error_code ec, std::size_t)
			{
				socket_.shutdown(tcp::socket::shutdown_send, ec);
				file_serializer_.reset();
				file_response_.reset();
				accept();
			});
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}

void http_worker::process_post(beast::string_view target)
{
	send_file("index.html");
}

void http_worker::check_deadline()
{
	try {
		// The deadline may have moved, so check it has really passed.
		if (request_deadline_.expiry() <= std::chrono::steady_clock::now())
		{
			// Close socket to cancel any outstanding operation.
			beast::error_code ec;
			socket_.close();

			// Sleep indefinitely until we're given a new deadline.
			request_deadline_.expires_at(
				std::chrono::steady_clock::time_point::max());
		}

		request_deadline_.async_wait(
			[this](beast::error_code)
			{
				check_deadline();
			});
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}

beast::string_view mime_type(beast::string_view path)
{
	using beast::iequals;
	auto const ext = [&path]
	{
		auto const pos = path.rfind(".");
		if (pos == beast::string_view::npos)
			return beast::string_view{};
		return path.substr(pos);
	}();
	if (iequals(ext, ".htm"))  return "text/html";
	if (iequals(ext, ".html")) return "text/html";
	if (iequals(ext, ".php"))  return "text/html";
	if (iequals(ext, ".css"))  return "text/css";
	if (iequals(ext, ".txt"))  return "text/plain";
	if (iequals(ext, ".js"))   return "application/javascript";
	if (iequals(ext, ".json")) return "application/json";
	if (iequals(ext, ".xml"))  return "application/xml";
	if (iequals(ext, ".swf"))  return "application/x-shockwave-flash";
	if (iequals(ext, ".flv"))  return "video/x-flv";
	if (iequals(ext, ".png"))  return "image/png";
	if (iequals(ext, ".jpe"))  return "image/jpeg";
	if (iequals(ext, ".jpeg")) return "image/jpeg";
	if (iequals(ext, ".jpg"))  return "image/jpeg";
	if (iequals(ext, ".gif"))  return "image/gif";
	if (iequals(ext, ".bmp"))  return "image/bmp";
	if (iequals(ext, ".ico"))  return "image/vnd.microsoft.icon";
	if (iequals(ext, ".tiff")) return "image/tiff";
	if (iequals(ext, ".tif"))  return "image/tiff";
	if (iequals(ext, ".svg"))  return "image/svg+xml";
	if (iequals(ext, ".svgz")) return "image/svg+xml";
	if (iequals(ext, ".woff2")) return "font/woff2";
	if (iequals(ext, ".woff")) return "font/woff";
	return "";
}
