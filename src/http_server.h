//
// Copyright (c) 2017 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//

//------------------------------------------------------------------------------
//
// Example: HTTP server, fast 
//
//------------------------------------------------------------------------------

#ifndef __HTTP_SERVER_H__
#define __HTTP_SERVER_H__

#include "html_parser/fields_alloc.h"

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio.hpp>
#include <map>
#include <boost/filesystem.hpp>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <list>
#include <memory>
#include <string>

#include "mvc/controller.h"
#include "mvc/model.h"
#include "mvc/view.h"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

// Return a reasonable mime type based on the extension of a file.
beast::string_view
mime_type(beast::string_view path);

class http_worker
{
public:
    http_worker(http_worker const&) = delete;
    http_worker& operator=(http_worker const&) = delete;

    http_worker(tcp::acceptor& acceptor, const std::string& doc_root) :
        acceptor_(acceptor),
        doc_root_(doc_root)
    {
		
    }

	int x = 1010101;

    void start()
    {
        accept();
        check_deadline();
    }

	struct set_cookies
	{
		std::map<std::string, std::string> values;

		set_cookies(std::string key, std::string val)
		{
			values.emplace(key, val);
		}

		set_cookies(const char* key, std::string val)
		{
			values.emplace(std::string(key), val);
		}

		set_cookies operator()(std::string key, std::string val)
		{
			values.emplace (key, val);
			return *this;
		}

		set_cookies operator()(const char* key, std::string val)
		{
			values.emplace(std::string(key), val);
			return *this;
		}
	};

private:
    using alloc_t = fields_alloc<char>;
    //using request_body_t = http::basic_dynamic_body<beast::flat_static_buffer<1024 * 1024>>;
    using request_body_t = http::dynamic_body;

    // The acceptor used to listen for incoming connections.
    tcp::acceptor& acceptor_;

    // The path to the root of the document directory.
    std::string doc_root_ = "htdocs/";

    // The socket for the currently connected client.
    tcp::socket socket_{acceptor_.get_executor()};

    // The buffer for performing reads
    beast::flat_static_buffer<8192> buffer_;

    // The allocator used for the fields in the request and reply.
    alloc_t alloc_{8192};

    // The parser for reading the requests
	boost::optional<http::request_parser<request_body_t, alloc_t>> parser_;

    // The timer putting a time limit on requests.
    net::basic_waitable_timer<std::chrono::steady_clock> request_deadline_{
        acceptor_.get_executor(), (std::chrono::steady_clock::time_point::max)()};

    // The string-based response message.
    boost::optional<http::response<http::string_body>> string_response_;
	boost::optional<http::response<http::empty_body>> empty_response_;

    // The string-based response serializer.
    boost::optional<http::response_serializer<http::string_body>> string_serializer_;
	// The string-based response serializer.
	boost::optional<http::response_serializer<http::empty_body>> empty_serializer_;

    // The file-based response message.
    boost::optional<http::response<http::file_body>> file_response_;

    // The file-based response serializer.
    boost::optional<http::response_serializer<http::file_body>> file_serializer_;

	void accept();

	void send_redirect(std::string to, set_cookies sc);
	void send_redirect(std::string to, bool delete_cookies = false);

	void send_view(http_server::view&);
    

	void read_request();
	void read_body();

	void process_request(http::request<request_body_t, http::basic_fields<alloc_t>> const& req);

	void send_bad_response(
		http::status status,
		std::string const& error);

	void send_file(beast::string_view target);

	void process_post(beast::string_view target);

	void check_deadline();
};


#endif