#include<memory>
#include<string>
#include <functional>

#include "mvc/model.h"
#include "mvc/controller.h"
#include "http_server.h"

class my_model_two : public http_server::model
{
private:
	std::string value = "hello from class 3 ";



public:
	my_model_two(std::string val) : model()
	{
		value = val;
		addAttribute("value", &value);
	}
};

class my_other_model : public http_server::model
{
private:
std::string value;
std::shared_ptr<my_model_two> m2;
std::vector<my_model_two*> models;

public:
	my_other_model() : model()
	{
		m2 = std::make_shared<my_model_two>("model_to_one");
		value = "this is the value";
		std::function<std::string()> f = std::bind(&my_other_model::getValue, this);
		addFunction("getValue", std::function<std::string()>(std::bind(&my_other_model::getValue, this)));
		addFunction("setValue", std::function < void(std::string)>(std::bind(&my_other_model::setValue, this, std::placeholders::_1)));
		models.push_back(new my_model_two("test"));
		models.push_back(new my_model_two("looool"));
		addAttribute("models", &models);

		addAttribute("m2", std::dynamic_pointer_cast<model_element>(m2));
	}

	std::string getValue()
	{
		return value;
	}

	void setValue(std::string val)
	{
		value = val;
	}
};


class my_model : public http_server::model
{
private:
	int x = 19990;
	double z = 208.11f;
	std::shared_ptr<my_other_model> m;

	std::string myFunction() { return m->getValue(); }

public:
	my_model() : model()
	{
		m = std::make_shared<my_other_model>();
		addAttribute("x", &x);
		addAttribute("z", &z);
		addAttribute("m", std::dynamic_pointer_cast<model_element>(m));
	}
};

class my_controller : public http_server::controller
{
private:
	std::shared_ptr< my_model> m;
public:
	my_controller() : controller()
	{
		m = std::make_shared < my_model>();
		addCallback({ "/", "/index" }, [&](http_server::model* model) -> std::string {
			model->addAttribute("my_model", std::dynamic_pointer_cast<http_server::model_element>(m));
			return "index";
		}, boost::beast::http::verb::get);

	}
};


int main()
{
	my_controller m;

	boost::asio::io_context ioc{ 1 };
	boost::asio::ip::tcp::acceptor acceptor(ioc,
		boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 81));

	int num_workers = 2;

	std::list<http_worker> workers;

	for (int i = 0; i < num_workers; ++i)
	{
		workers.emplace_back(acceptor, "htdocs/");
		workers.back().start();
	}

	ioc.run();
	return 0;
}