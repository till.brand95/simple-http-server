<!-- PROJECT LOGO -->
<br />

  <h3 align="center">Simple Http Server</h3>

  <p align="center">
    A Simple c++ MVC Framework
    <br />
	</p>




<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project

In case of web server frameworks for C++ there are very view. 
treefrogg -> uses QT
nvmpp

Noneof them suieted my wishes so i decided to start my own. This is a quick version. I try to keep it updated as is move. 

### Requirements

* Windows/Unix
* C++17 Capable Cmpiler
* [Boost 1.70](https://www.boost.org/)
* CMake

## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Installation Unix

1. Install or/build Boost https://www.boost.org/
2. run Terminal
```sh
#clone the repo
git clone https://github.com/your_username_/Project-Name.git
cd simple-http-server
cmake .
make
```

<!-- USAGE EXAMPLES -->
## Usage


_For a example, please refer to the [orderU-server](https://gitlab.com/till.brand95/orderu-server)_

## Example

    #include<memory>
    #include<string>
    #include <functional>
    
    #include "mvc/model.h"
    #include "mvc/controller.h"
    #include "http_server.h"
    
    class my_model_two : public http_server::model
    {
    private:
    	std::string value = "hello from class 3 ";
    
    
    
    public:
    	my_model_two(std::string val) : model()
    	{
    		value = val;
    		addAttribute("value", &value);
    	}
    };
    
    class my_other_model : public http_server::model
    {
    private:
    std::string value;
    std::shared_ptr<my_model_two> m2;
    std::vector<my_model_two*> models;
    
    public:
    	my_other_model() : model()
    	{
    		m2 = std::make_shared<my_model_two>("model_to_one");
    		value = "this is the value";
    		std::function<std::string()> f = std::bind(&my_other_model::getValue, this);
    		addFunction("getValue", std::function<std::string()>(std::bind(&my_other_model::getValue, this)));
    		addFunction("setValue", std::function < void(std::string)>(std::bind(&my_other_model::setValue, this, std::placeholders::_1)));
    		models.push_back(new my_model_two("test"));
    		models.push_back(new my_model_two("looool"));
    		addAttribute("models", &models);
    
    		addAttribute("m2", std::dynamic_pointer_cast<model_element>(m2));
    	}
    
    	std::string getValue()
    	{
    		return value;
    	}
    
    	void setValue(std::string val)
    	{
    		value = val;
    	}
    };
    
    
    class my_model : public http_server::model
    {
    private:
    	int x = 19990;
    	double z = 208.11f;
    	std::shared_ptr<my_other_model> m;
    
    	std::string myFunction() { return m->getValue(); }
    
    public:
    	my_model() : model()
    	{
    		m = std::make_shared<my_other_model>();
    		addAttribute("x", &x);
    		addAttribute("z", &z);
    		addAttribute("m", std::dynamic_pointer_cast<model_element>(m));
    	}
    };
    
    class my_controller : public http_server::controller
    {
    private:
    	std::shared_ptr< my_model> m;
    public:
    	my_controller() : controller()
    	{
    		m = std::make_shared < my_model>();
    		addCallback({ "/", "/index" }, [&](http_server::model* model) -> std::string {
    			model->addAttribute("my_model", std::dynamic_pointer_cast<http_server::model_element>(m));
    			return "index";
    		}, boost::beast::http::verb::get);
    
    	}
    };
    
    
    int main()
    {
    	my_controller m;
    
    	boost::asio::io_context ioc{ 1 };
    	boost::asio::ip::tcp::acceptor acceptor(ioc,
    		boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 81));
    
    	int num_workers = 2;
    
    	std::list<http_worker> workers;
    
    	for (int i = 0; i < num_workers; ++i)
    	{
    		workers.emplace_back(acceptor, "htdocs/");
    		workers.back().start();
    	}
    
    	ioc.run();
    	return 0;
    }
    									
    ##<bin_dir>/htdocs/views/index.html
    <html>
    	<head>
    		
    	</head>
    	<body>
    		<h1>This is a test <% my_model.x %> </h1><br>
    			Mymodel z: <% my_model.z %> <br>
    			Set new variable q to mymodel m  <br>
    			Func get value q:<% SET q = my_model.m %> <% SET a = q.getValue() %><% a %><% a = "test" %> <br>
    			Func set value q: <% q.setValue("hello world") %>
    			<% q.getValue() %> <br>
    			<% q.setValue(a) %>
    			<% q.getValue() %> <br>
    			if mymodel z: <% IF my_model.z %% true %% ELSE %% false	%% ENDIF %><br>
    			<b>For Loop:</b><br>
    			<% FOR element : my_model.m.models %% <% element.value %><br> %% ENDFOR %>
    	</body>
    </html>




<!-- ROADMAP -->
## Roadmap




<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Boost](https://www.boost.org/)